#!/bin/bash
xmllint --noout --dtdvalid nation.dtd nation-curacao.xml
xmllint --noout --dtdvalid nation.dtd nation-iraq.xml
xmllint --noout --dtdvalid nation.dtd nation-new-zealand.xml
xmllint --noout --dtdvalid nation.dtd nation-taiwan.xml
trang nation.rnc nation.rng
xmllint --noout --relaxng nation.rng nation-curacao.xml
xmllint --noout --relaxng nation.rng nation-iraq.xml
xmllint --noout --relaxng nation.rng nation-new-zealand.xml
xmllint --noout --relaxng nation.rng nation-taiwan.xml
sh ./clean.sh
sh ./build-html.sh
sh ./build-pdf.sh
sh ./build-epub.sh
