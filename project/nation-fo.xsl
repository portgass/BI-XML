<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="2.0" 
	xmlns:xsl="http://www.w3.org/1999/XSL/Transform" 
	xmlns:fo="http://www.w3.org/1999/XSL/Format">
	<xsl:output method="xml" indent="yes"/>
	<xsl:template match="/">
		<fo:root>
			<fo:layout-master-set>
				<fo:simple-page-master master-name="A4" page-height="29.7cm" page-width="21.0cm" margin="0.5cm">
					<fo:region-body margin="1cm" />
                    <fo:region-before extent="1cm" />
                    <fo:region-after extent="0.7cm" />
				</fo:simple-page-master>
			</fo:layout-master-set>

			<fo:page-sequence master-reference="A4">
				<fo:static-content flow-name="xsl-region-before">
                    <fo:block font-size="8pt">
                        Semestrální projekt - BI-XML

                        <fo:leader leader-pattern="space" leader-length="14.4cm" />

                        Petr Svoboda
                    </fo:block>
				</fo:static-content>
				<fo:static-content flow-name="xsl-region-after">
					<fo:block font-size="8pt" text-align="right">
        				Strana 

						<fo:page-number/>/<fo:page-number-citation ref-id="last-page"/>
					</fo:block>
				</fo:static-content>

				<fo:flow flow-name="xsl-region-body">
					<fo:block>

						<xsl:apply-templates select="document('nation-curacao.xml')/nation" />
                        <xsl:apply-templates select="document('nation-iraq.xml')/nation" />
                        <xsl:apply-templates select="document('nation-new-zealand.xml')/nation" />
                        <xsl:apply-templates select="document('nation-taiwan.xml')/nation" />

                        <fo:block id="last-page" />

					</fo:block>
				</fo:flow>
			</fo:page-sequence>
		</fo:root>
	</xsl:template>

    <xsl:template match="nation">
        <fo:block break-before="page">
            <fo:table table-layout="fixed" width="100%">
                <fo:table-column column-width="65%" />
                <fo:table-column column-width="35%" />

                <fo:table-body>

                    <fo:table-row>
                        <fo:table-cell>

                            <fo:block id="{@id}-title" font-size="26pt" font-weight="bold" margin-bottom="16pt">
                                <xsl:value-of select="@name" />
                            </fo:block>

                            <fo:list-block id="{@id}-contents" margin-bottom="8pt">
                                <fo:list-item>
                                    <fo:list-item-label width="0.2cm">
                                        <fo:block>-</fo:block>
                                    </fo:list-item-label>

                                    <fo:list-item-body start-indent="8pt">
                                        <fo:block>
                                            <fo:basic-link internal-destination="{@id}-title">
                                                Title
                                            </fo:basic-link>
                                        </fo:block>
                                    </fo:list-item-body>
                                </fo:list-item>
                                <fo:list-item>
                                    <fo:list-item-label width="0.2cm">
                                        <fo:block>-</fo:block>
                                    </fo:list-item-label>

                                    <fo:list-item-body start-indent="8pt">
                                        <fo:block>
                                            <fo:basic-link internal-destination="{@id}-contents">
                                                Contents
                                            </fo:basic-link>
                                        </fo:block>
                                    </fo:list-item-body>
                                </fo:list-item>
                                <fo:list-item>
                                    <fo:list-item-label width="0.2cm">
                                        <fo:block>-</fo:block>
                                    </fo:list-item-label>

                                    <fo:list-item-body start-indent="8pt">
                                        <fo:block>
                                            <fo:basic-link internal-destination="{@id}-background">
                                                Background
                                            </fo:basic-link>
                                        </fo:block>
                                    </fo:list-item-body>
                                </fo:list-item>
                                <fo:list-item>
                                    <fo:list-item-label width="0.2cm">
                                        <fo:block>-</fo:block>
                                    </fo:list-item-label>

                                    <fo:list-item-body start-indent="8pt">
                                        <fo:block>
                                            <fo:basic-link internal-destination="{@id}-geography">
                                                Geography
                                            </fo:basic-link>
                                        </fo:block>
                                    </fo:list-item-body>
                                </fo:list-item>
                                <fo:list-item>
                                    <fo:list-item-label width="0.2cm">
                                        <fo:block>-</fo:block>
                                    </fo:list-item-label>

                                    <fo:list-item-body start-indent="8pt">
                                        <fo:block>
                                            <fo:basic-link internal-destination="{@id}-society">
                                                People and society
                                            </fo:basic-link>
                                        </fo:block>
                                    </fo:list-item-body>
                                </fo:list-item>
                                <fo:list-item>
                                    <fo:list-item-label width="0.2cm">
                                        <fo:block>-</fo:block>
                                    </fo:list-item-label>

                                    <fo:list-item-body start-indent="8pt">
                                        <fo:block>
                                            <fo:basic-link internal-destination="{@id}-government">
                                                Government
                                            </fo:basic-link>
                                        </fo:block>
                                    </fo:list-item-body>
                                </fo:list-item>
                                <fo:list-item>
                                    <fo:list-item-label width="0.2cm">
                                        <fo:block>-</fo:block>
                                    </fo:list-item-label>

                                    <fo:list-item-body start-indent="8pt">
                                        <fo:block>
                                            <fo:basic-link internal-destination="{@id}-economy">
                                                Economy
                                            </fo:basic-link>
                                        </fo:block>
                                    </fo:list-item-body>
                                </fo:list-item>
                                <fo:list-item>
                                    <fo:list-item-label width="0.2cm">
                                        <fo:block>-</fo:block>
                                    </fo:list-item-label>

                                    <fo:list-item-body start-indent="8pt">
                                        <fo:block>
                                            <fo:basic-link internal-destination="{@id}-energy">
                                                Energy
                                            </fo:basic-link>
                                        </fo:block>
                                    </fo:list-item-body>
                                </fo:list-item>
                                <fo:list-item>
                                    <fo:list-item-label width="0.2cm">
                                        <fo:block>-</fo:block>
                                    </fo:list-item-label>

                                    <fo:list-item-body start-indent="8pt">
                                        <fo:block>
                                            <fo:basic-link internal-destination="{@id}-communications">
                                                Communications
                                            </fo:basic-link>
                                        </fo:block>
                                    </fo:list-item-body>
                                </fo:list-item>
                                <fo:list-item>
                                    <fo:list-item-label width="0.2cm">
                                        <fo:block>-</fo:block>
                                    </fo:list-item-label>

                                    <fo:list-item-body start-indent="8pt">
                                        <fo:block>
                                            <fo:basic-link internal-destination="{@id}-transportation">
                                                Transportation
                                            </fo:basic-link>
                                        </fo:block>
                                    </fo:list-item-body>
                                </fo:list-item>
                                <fo:list-item>
                                    <fo:list-item-label width="0.2cm">
                                        <fo:block>-</fo:block>
                                    </fo:list-item-label>

                                    <fo:list-item-body start-indent="8pt">
                                        <fo:block>
                                            <fo:basic-link internal-destination="{@id}-military-and-security">
                                                Military and security
                                            </fo:basic-link>
                                        </fo:block>
                                    </fo:list-item-body>
                                </fo:list-item>
                            </fo:list-block>

                            <fo:block id="{@id}-background" font-size="16pt" font-weight="bold" margin-bottom="8pt">
                                Background
                            </fo:block>

                            <fo:block font-size="10pt" text-align="justify" margin-bottom="16pt">
                                <xsl:value-of select="introduction/background" />
                            </fo:block>

                            <xsl:apply-templates select="geography">
                                <xsl:with-param name="id" select="@id" />
                            </xsl:apply-templates>
                            <xsl:apply-templates select="people-and-society">
                                <xsl:with-param name="id" select="@id" />
                            </xsl:apply-templates>
                            <xsl:apply-templates select="government">
                                <xsl:with-param name="id" select="@id" />
                            </xsl:apply-templates>
                            <xsl:apply-templates select="economy">
                                <xsl:with-param name="id" select="@id" />
                            </xsl:apply-templates>
                            <xsl:apply-templates select="energy">
                                <xsl:with-param name="id" select="@id" />
                            </xsl:apply-templates>
                            <xsl:apply-templates select="communications">
                                <xsl:with-param name="id" select="@id" />
                            </xsl:apply-templates>
                            <xsl:apply-templates select="transportation">
                                <xsl:with-param name="id" select="@id" />
                            </xsl:apply-templates>
                            <xsl:apply-templates select="military-and-security">
                                <xsl:with-param name="id" select="@id" />
                            </xsl:apply-templates>

                        </fo:table-cell>
                        <fo:table-cell>

                            <fo:block text-align="right">
                                <fo:external-graphic
                                    src="images/{@name}.gif"
                                    content-width="160pt"
                                />
                            </fo:block>

                        </fo:table-cell>
                    </fo:table-row>

                </fo:table-body>
            </fo:table>
        </fo:block>
    </xsl:template>

    <xsl:template name="block-1">
        <xsl:param name="element" />
        <xsl:param name="children" />
        <xsl:variable name="element-name">
            <xsl:value-of select="translate(local-name($element), '-', ' ')" />
        </xsl:variable>

        <fo:block>
                
            <fo:block text-transform="capitalize" font-size="13pt" font-weight="bold" margin-top="8pt" margin-bottom="2pt">
                <xsl:value-of select="$element-name" />
            </fo:block>

            <xsl:if test="$element/country-comparison-to-the-world">
                <xsl:call-template name="world-compare">
                    <xsl:with-param name="rank" select="$element/country-comparison-to-the-world" />
                </xsl:call-template>
            </xsl:if>

            <xsl:choose>
                <xsl:when test="$children">
                    <xsl:for-each select="$children[not(self::country-comparison-to-the-world)]">
                        <xsl:call-template name="block-2">
                            <xsl:with-param name="element" select="." />
                        </xsl:call-template>
                    </xsl:for-each>
                </xsl:when>
                <xsl:otherwise>
                    <fo:block font-size="10pt" margin-bottom="2pt">
                        <xsl:value-of select="$element" />
                    </fo:block>
                </xsl:otherwise>
            </xsl:choose>

        </fo:block>
    </xsl:template>

    <xsl:template name="block-2">
        <xsl:param name="element" />
        <xsl:param name="children" />
        <xsl:variable name="element-name">
            <xsl:value-of select="translate(local-name($element), '-', ' ')" />
        </xsl:variable>

        <fo:block>
                
            <fo:block text-transform="capitalize" font-size="10pt" font-weight="bold" margin-top="4pt" margin-bottom="2pt">
                <xsl:value-of select="$element-name" />
            </fo:block>
            <fo:block font-size="10pt" margin-bottom="2pt">
                <xsl:value-of select="$element" />
            </fo:block>

        </fo:block>
    </xsl:template>
    
    <xsl:template name="block-list">
        <xsl:param name="element" />
        <xsl:variable name="element-name">
            <xsl:value-of select="translate(local-name($element), '-', ' ')" />
        </xsl:variable>

        <fo:block>
                
            <fo:block text-transform="capitalize" font-size="13pt" font-weight="bold" margin-top="8pt" margin-bottom="2pt">
                <xsl:value-of select="$element-name" />
            </fo:block>

            <fo:list-block>
                <xsl:for-each select="$element/*">

                    <fo:list-item>
                        <fo:list-item-label width="0.2cm">
                            <fo:block>-</fo:block>
                        </fo:list-item-label>
                        <fo:list-item-body start-indent="8pt">
                            <fo:block>
                                <xsl:value-of select="." />
                            </fo:block>
                        </fo:list-item-body>
                    </fo:list-item>

                </xsl:for-each>
            </fo:list-block>

        </fo:block>
    </xsl:template>

    <xsl:template name="world-compare">
        <xsl:param name="rank" />
                
        <fo:block font-size="8pt" margin-bottom="2pt">
            World rank: <xsl:value-of select="$rank" />
        </fo:block>
    </xsl:template>

    <xsl:template match="geography">
        <xsl:param name="id" />
        <fo:block break-before="page">
            <fo:block id="{$id}-geography" font-size="16pt" font-weight="bold" margin-top="12pt">
                Geography
            </fo:block>

            <xsl:call-template name="block-1">
                <xsl:with-param name="element" select="location" />
            </xsl:call-template>
            <xsl:call-template name="block-1">
                <xsl:with-param name="element" select="geographic-coordinates" />
            </xsl:call-template>
            <xsl:call-template name="block-1">
                <xsl:with-param name="element" select="map-references" />
            </xsl:call-template>
            <xsl:call-template name="block-1">
                <xsl:with-param name="element" select="area" />
                <xsl:with-param name="children" select="area/*" />
            </xsl:call-template>
            <xsl:call-template name="block-1">
                <xsl:with-param name="element" select="area-comparative" />
            </xsl:call-template>
            <xsl:call-template name="block-1">
                <xsl:with-param name="element" select="land-boundaries" />
            </xsl:call-template>
            <xsl:call-template name="block-1">
                <xsl:with-param name="element" select="coastline" />
            </xsl:call-template>
            <xsl:call-template name="block-1">
                <xsl:with-param name="element" select="maritime-claims" />
                <xsl:with-param name="children" select="maritime-claims/*" />
            </xsl:call-template>
            <xsl:call-template name="block-1">
                <xsl:with-param name="element" select="climate" />
            </xsl:call-template>
            <xsl:call-template name="block-1">
                <xsl:with-param name="element" select="terrain" />
            </xsl:call-template>
            <xsl:call-template name="block-1">
                <xsl:with-param name="element" select="elevation" />
                <xsl:with-param name="children" select="elevation/*" />
            </xsl:call-template>
            <xsl:call-template name="block-list">
                <xsl:with-param name="element" select="natural-resources" />
            </xsl:call-template>
            <xsl:apply-templates select="land-use"/>
            <xsl:call-template name="block-1">
                <xsl:with-param name="element" select="irrigated-land" />
            </xsl:call-template>
            <xsl:call-template name="block-1">
                <xsl:with-param name="element" select="population-distribution" />
            </xsl:call-template>
            <xsl:call-template name="block-1">
                <xsl:with-param name="element" select="natural-hazards" />
            </xsl:call-template>
            <xsl:call-template name="block-1">
                <xsl:with-param name="element" select="environment-current-issues" />
            </xsl:call-template>
            <xsl:call-template name="block-1">
                <xsl:with-param name="element" select="geography-note" />
            </xsl:call-template>
        </fo:block>
    </xsl:template>

    <xsl:template match="people-and-society">
        <xsl:param name="id" />
        <fo:block break-before="page">
            <fo:block id="{$id}-society" font-size="16pt" font-weight="bold" margin-top="12pt">
                Society
            </fo:block>

            <xsl:call-template name="block-1">
                <xsl:with-param name="element" select="population" />
            </xsl:call-template>
            <xsl:call-template name="block-1">
                <xsl:with-param name="element" select="nationality" />
                <xsl:with-param name="children" select="nationality/*" />
            </xsl:call-template>
            <xsl:apply-templates select="ethnic-groups"/>
            <xsl:apply-templates select="languages"/>
            <xsl:apply-templates select="religions"/>
            <xsl:apply-templates select="age"/>
            <fo:block>
                
                <fo:block font-size="10pt" font-weight="bold" margin-top="4pt" margin-bottom="2pt">
                    Growth rate
                </fo:block>
                <fo:block font-size="10pt" margin-bottom="2pt">
                    <xsl:value-of select="growth-rate/@percentage" />
                </fo:block>

            </fo:block>
            <fo:block>
                
                <fo:block font-size="10pt" font-weight="bold" margin-top="4pt" margin-bottom="2pt">
                    Birth rate
                </fo:block>
                <fo:block font-size="10pt" margin-bottom="2pt">
                    <xsl:value-of select="birth-rate/@amount" />&#160;<xsl:value-of select="birth-rate/@unit" />
                </fo:block>

            </fo:block>
            <fo:block>
                
                <fo:block font-size="10pt" font-weight="bold" margin-top="4pt" margin-bottom="2pt">
                    Death rate
                </fo:block>
                <fo:block font-size="10pt" margin-bottom="2pt">
                    <xsl:value-of select="death-rate/@amount" />&#160;<xsl:value-of select="death-rate/@unit" />
                </fo:block>

            </fo:block>
            <fo:block>
                
                <fo:block font-size="10pt" font-weight="bold" margin-top="4pt" margin-bottom="2pt">
                    Migration rate
                </fo:block>
                <fo:block font-size="10pt" margin-bottom="2pt">
                    <xsl:value-of select="migration-rate/@amount" />&#160;<xsl:value-of select="migration-rate/@unit" />
                </fo:block>

            </fo:block>
            <xsl:call-template name="block-1">
                <xsl:with-param name="element" select="population-distribution" />
            </xsl:call-template>
            <xsl:call-template name="block-1">
                <xsl:with-param name="element" select="major-urban-areas-population" />
            </xsl:call-template>
            <xsl:apply-templates select="sex-ratios"/>
            <xsl:call-template name="block-1">
                <xsl:with-param name="element" select="infant-mortality-rate" />
                <xsl:with-param name="children" select="infant-mortality-rate/*" />
            </xsl:call-template>
            <xsl:call-template name="block-1">
                <xsl:with-param name="element" select="life-expectancy-at-birth" />
                <xsl:with-param name="children" select="life-expectancy-at-birth/*" />
            </xsl:call-template>
            <xsl:call-template name="block-1">
                <xsl:with-param name="element" select="total-fertility-rate" />
            </xsl:call-template>
        </fo:block>
    </xsl:template>

    <xsl:template match="government">
        <xsl:param name="id" />
        <fo:block break-before="page">
            <fo:block id="{$id}-government" font-size="16pt" font-weight="bold" margin-top="12pt">
                Government
            </fo:block>

            <xsl:call-template name="block-1">
                <xsl:with-param name="element" select="country-name" />
                <xsl:with-param name="children" select="country-name/*" />
            </xsl:call-template>
            <xsl:call-template name="block-1">
                <xsl:with-param name="element" select="government-type" />
            </xsl:call-template>
            <xsl:call-template name="block-1">
                <xsl:with-param name="element" select="capital" />
                <xsl:with-param name="children" select="capital/*" />
            </xsl:call-template>
            <xsl:call-template name="block-1">
                <xsl:with-param name="element" select="administrative-divisions" />
            </xsl:call-template>
            <xsl:call-template name="block-1">
                <xsl:with-param name="element" select="national-holiday" />
            </xsl:call-template>
            <xsl:call-template name="block-1">
                <xsl:with-param name="element" select="constitution" />
            </xsl:call-template>
            <xsl:call-template name="block-1">
                <xsl:with-param name="element" select="legal-system" />
            </xsl:call-template>
            <xsl:call-template name="block-1">
                <xsl:with-param name="element" select="suffrage" />
            </xsl:call-template>
            <xsl:call-template name="block-1">
                <xsl:with-param name="element" select="executive-branch" />
                <xsl:with-param name="children" select="executive-branch/*" />
            </xsl:call-template>
            <xsl:call-template name="block-1">
                <xsl:with-param name="element" select="legislative-branch" />
                <xsl:with-param name="children" select="legislative-branch/*" />
            </xsl:call-template>
            <xsl:call-template name="block-1">
                <xsl:with-param name="element" select="judicial-branch" />
                <xsl:with-param name="children" select="judicial-branch/*" />
            </xsl:call-template>
            <xsl:call-template name="block-1">
                <xsl:with-param name="element" select="political-parties-and-leaders" />
            </xsl:call-template>
            <xsl:call-template name="block-1">
                <xsl:with-param name="element" select="flag-description" />
            </xsl:call-template>
            <xsl:call-template name="block-1">
                <xsl:with-param name="element" select="national-symbol" />
            </xsl:call-template>
            <xsl:call-template name="block-1">
                <xsl:with-param name="element" select="national-anthem" />
                <xsl:with-param name="children" select="national-anthem/*" />
            </xsl:call-template>
        </fo:block>
    </xsl:template>

    <xsl:template match="economy">
        <xsl:param name="id" />
        <fo:block break-before="page">
            <fo:block id="{$id}-economy" font-size="16pt" font-weight="bold" margin-top="12pt">
                Economy
            </fo:block>

            <xsl:call-template name="block-1">
                <xsl:with-param name="element" select="economy-overview" />
            </xsl:call-template>
            <xsl:call-template name="block-1">
                <xsl:with-param name="element" select="gdp-purchasing-power-parity" />
            </xsl:call-template>
            <xsl:call-template name="block-1">
                <xsl:with-param name="element" select="gdp-official-exchange-rate" />
            </xsl:call-template>
            <xsl:call-template name="block-1">
                <xsl:with-param name="element" select="gdp-real-growth-rate" />
            </xsl:call-template>
            <xsl:call-template name="block-1">
                <xsl:with-param name="element" select="gdp-per-capita" />
            </xsl:call-template>
            <xsl:call-template name="block-1">
                <xsl:with-param name="element" select="by-end-use" />
                <xsl:with-param name="children" select="by-end-use/*" />
            </xsl:call-template>
            <xsl:call-template name="block-1">
                <xsl:with-param name="element" select="by-sector-of-origin" />
                <xsl:with-param name="children" select="by-sector-of-origin/*" />
            </xsl:call-template>
            <xsl:call-template name="block-list">
                <xsl:with-param name="element" select="agriculture-products" />
            </xsl:call-template>
            <xsl:call-template name="block-list">
                <xsl:with-param name="element" select="industries" />
            </xsl:call-template>
            <xsl:call-template name="block-1">
                <xsl:with-param name="element" select="labor-force" />
            </xsl:call-template>
            <xsl:call-template name="block-1">
                <xsl:with-param name="element" select="labor-force-by-occupation" />
                <xsl:with-param name="children" select="labor-force-by-occupation/*" />
            </xsl:call-template>
            <xsl:call-template name="block-1">
                <xsl:with-param name="element" select="unemployment-rate" />
            </xsl:call-template>
            <xsl:call-template name="block-1">
                <xsl:with-param name="element" select="taxes-and-other-revenues" />
            </xsl:call-template>
            <xsl:call-template name="block-1">
                <xsl:with-param name="element" select="public-debt" />
            </xsl:call-template>
            <xsl:call-template name="block-1">
                <xsl:with-param name="element" select="inflation-rate-consumer-prices" />
            </xsl:call-template>
            <xsl:call-template name="block-1">
                <xsl:with-param name="element" select="exports" />
            </xsl:call-template>
            <xsl:call-template name="block-1">
                <xsl:with-param name="element" select="exports-commodities" />
            </xsl:call-template>
            <xsl:call-template name="block-1">
                <xsl:with-param name="element" select="imports" />
            </xsl:call-template>
            <xsl:call-template name="block-1">
                <xsl:with-param name="element" select="imports-commodities" />
            </xsl:call-template>
            <xsl:call-template name="block-1">
                <xsl:with-param name="element" select="exchange-rates" />
            </xsl:call-template>
        </fo:block>
    </xsl:template>

    <xsl:template match="energy">
        <xsl:param name="id" />
        <fo:block break-before="page">
            <fo:block id="{$id}-energy" font-size="16pt" font-weight="bold" margin-top="12pt">
                Energy
            </fo:block>

            <xsl:for-each select="./*">
                <xsl:call-template name="block-1">
                    <xsl:with-param name="element" select="." />
                </xsl:call-template>
            </xsl:for-each>
            
        </fo:block>
    </xsl:template>

    <xsl:template match="communications">
        <xsl:param name="id" />
        <fo:block break-before="page">
            <fo:block id="{$id}-communications" font-size="16pt" font-weight="bold" margin-top="12pt">
                Communications
            </fo:block>

            <xsl:call-template name="block-1">
                <xsl:with-param name="element" select="telephone-system" />
            </xsl:call-template>
            <xsl:call-template name="block-1">
                <xsl:with-param name="element" select="broadcast-media" />
            </xsl:call-template>
            <xsl:call-template name="block-1">
                <xsl:with-param name="element" select="internet-country-code" />
            </xsl:call-template>
            <xsl:call-template name="block-1">
                <xsl:with-param name="element" select="internet-users" />
                <xsl:with-param name="children" select="internet-users/*" />
            </xsl:call-template>
        </fo:block>
    </xsl:template>

    <xsl:template match="transportation">
        <xsl:param name="id" />
        <fo:block break-before="page">
            <fo:block id="{$id}-transportation" font-size="16pt" font-weight="bold" margin-top="12pt">
                Transportation
            </fo:block>

            <xsl:call-template name="block-1">
                <xsl:with-param name="element" select="national-air-transport-system" />
                <xsl:with-param name="children" select="national-air-transport-system/*" />
            </xsl:call-template>
            <xsl:call-template name="block-1">
                <xsl:with-param name="element" select="civil-aircraft-registration-country-code-prefix" />
            </xsl:call-template>
            <xsl:call-template name="block-1">
                <xsl:with-param name="element" select="airports" />
                <xsl:with-param name="children" select="airports/*" />
            </xsl:call-template>
            <xsl:apply-templates select="airports-with-paved-runways"/>
            <xsl:call-template name="block-1">
                <xsl:with-param name="element" select="railways" />
                <xsl:with-param name="children" select="railways/*" />
            </xsl:call-template>
            <xsl:call-template name="block-1">
                <xsl:with-param name="element" select="roadways" />
                <xsl:with-param name="children" select="roadways/*" />
            </xsl:call-template>
            <xsl:call-template name="block-1">
                <xsl:with-param name="element" select="merchant-marine" />
                <xsl:with-param name="children" select="merchant-marine/*" />
            </xsl:call-template>
            <xsl:call-template name="block-1">
                <xsl:with-param name="element" select="ports-and-terminals" />
                <xsl:with-param name="children" select="ports-and-terminals/*" />
            </xsl:call-template>
        </fo:block>
    </xsl:template>

    <xsl:template match="military-and-security">
        <xsl:param name="id" />
        <fo:block break-before="page">
            <fo:block id="{$id}-military-and-security" font-size="16pt" font-weight="bold" margin-top="12pt">
                Military and security
            </fo:block>

            <xsl:call-template name="block-1">
                <xsl:with-param name="element" select="military-branches" />
            </xsl:call-template>
            <xsl:call-template name="block-1">
                <xsl:with-param name="element" select="military-service-age-and-obligation" />
            </xsl:call-template>
        </fo:block>
    </xsl:template>

    <xsl:template match="land-use">
        <fo:block font-size="13pt" font-weight="bold" margin-bottom="2pt">
            Land use
        </fo:block>

        <fo:list-block margin-bottom="8pt">
            <xsl:for-each select="land-use-category">

                <fo:list-item>
                    <fo:list-item-label width="0.2cm">
                        <fo:block>-</fo:block>
                    </fo:list-item-label>
                    <fo:list-item-body start-indent="8pt">
                        <fo:block>
                            <xsl:value-of select="@type" />: <xsl:value-of select="@percentage" />%
                        </fo:block>
                    </fo:list-item-body>
                </fo:list-item>

            </xsl:for-each>
        </fo:list-block>
    </xsl:template>

    <xsl:template match="sex-ratios">
        <fo:block font-size="13pt" font-weight="bold" margin-bottom="2pt">
            Sex ratios
        </fo:block>

        <fo:list-block margin-bottom="8pt">
            <xsl:for-each select="sex-ratio">

                <fo:list-item>
                    <fo:list-item-label width="0.2cm">
                        <fo:block>-</fo:block>
                    </fo:list-item-label>
                    <fo:list-item-body start-indent="8pt">
                        <fo:block>
                            <xsl:value-of select="@type" />: <xsl:value-of select="@amount" />
                        </fo:block>
                    </fo:list-item-body>
                </fo:list-item>

            </xsl:for-each>
        </fo:list-block>
    </xsl:template>

    <xsl:template match="airports-with-paved-runways">
        <fo:block font-size="13pt" font-weight="bold" margin-bottom="2pt">
            Airports with paved runways
        </fo:block>

        <fo:list-block margin-bottom="8pt">
            <xsl:for-each select="airport">

                <fo:list-item>
                    <fo:list-item-label width="0.2cm">
                        <fo:block>-</fo:block>
                    </fo:list-item-label>
                    <fo:list-item-body start-indent="8pt">
                        <fo:block>
                            <xsl:value-of select="@type" />: <xsl:value-of select="@amount" />
                        </fo:block>
                    </fo:list-item-body>
                </fo:list-item>

            </xsl:for-each>
        </fo:list-block>
    </xsl:template>

    <xsl:template match="ethnic-groups">
        <fo:block font-size="13pt" font-weight="bold" margin-bottom="2pt">
            Ethnic groups
        </fo:block>

        <fo:list-block margin-bottom="8pt">
            <xsl:for-each select="ethnic-group">

                <fo:list-item>
                    <fo:list-item-label width="0.2cm">
                        <fo:block>-</fo:block>
                    </fo:list-item-label>
                    <fo:list-item-body start-indent="8pt">
                        <fo:block>
                            <xsl:value-of select="@name" />
                            <xsl:if test="@percentage">
                                : <xsl:value-of select="@percentage" />%
                            </xsl:if>
                            <xsl:if test="@majority='true'">
                                (majority)
                            </xsl:if>
                        </fo:block>
                    </fo:list-item-body>
                </fo:list-item>

            </xsl:for-each>
        </fo:list-block>
    </xsl:template>

    <xsl:template match="languages">
        <fo:block font-size="13pt" font-weight="bold" margin-bottom="2pt">
            Languages
        </fo:block>

        <fo:list-block margin-bottom="8pt">
            <xsl:for-each select="language">

                <fo:list-item>
                    <fo:list-item-label width="0.2cm">
                        <fo:block>-</fo:block>
                    </fo:list-item-label>
                    <fo:list-item-body start-indent="8pt">
                        <fo:block>
                            <xsl:value-of select="@name" />
                            <xsl:if test="@percentage">
                                : <xsl:value-of select="@percentage" />%
                            </xsl:if>
                            <xsl:if test="@official='true'">
                                (official)
                            </xsl:if>
                        </fo:block>
                    </fo:list-item-body>
                </fo:list-item>

            </xsl:for-each>
        </fo:list-block>
    </xsl:template>

    <xsl:template match="religions">
        <fo:block font-size="13pt" font-weight="bold" margin-bottom="2pt">
            Religions
        </fo:block>

        <fo:list-block margin-bottom="8pt">
            <xsl:for-each select="religion">

                <fo:list-item>
                    <fo:list-item-label width="0.2cm">
                        <fo:block>-</fo:block>
                    </fo:list-item-label>
                    <fo:list-item-body start-indent="8pt">
                        <fo:block>
                            <xsl:value-of select="@name" />
                            <xsl:if test="@percentage">
                                : <xsl:value-of select="@percentage" />%
                            </xsl:if>
                            <xsl:if test="@official='true'">
                                (official)
                            </xsl:if>
                            
                            <xsl:if test="branch">
                                <fo:list-block>
                                    <xsl:for-each select="branch">

                                        <fo:list-item>
                                            <fo:list-item-label width="0.2cm">
                                                <fo:block>-</fo:block>
                                            </fo:list-item-label>
                                            <fo:list-item-body start-indent="16pt">
                                                <fo:block>
                                                    <xsl:value-of select="@name" />: <xsl:value-of select="@percentage" />%
                                                </fo:block>
                                            </fo:list-item-body>
                                        </fo:list-item>

                                    </xsl:for-each>
                                </fo:list-block>
                            </xsl:if>
                        </fo:block>
                    </fo:list-item-body>
                </fo:list-item>

            </xsl:for-each>
        </fo:list-block>
    </xsl:template>

    <xsl:template match="age">
        <fo:block font-size="13pt" font-weight="bold" margin-bottom="2pt">
            Age
        </fo:block>

        <fo:block font-size="10pt" font-weight="bold" margin-bottom="2pt">
            Median
        </fo:block>

        <fo:block font-size="10pt">
            Total: <xsl:value-of select="median/@total" />
        </fo:block>
        <fo:block font-size="10pt">
            Male: <xsl:value-of select="median/@male" />
        </fo:block>
        <fo:block font-size="10pt" margin-bottom="4pt">
            Female: <xsl:value-of select="median/@female" />
        </fo:block>

        <fo:block font-size="10pt" font-weight="bold" margin-bottom="2pt">
            Structure
        </fo:block>

        <fo:list-block margin-bottom="8pt">
            <xsl:for-each select="age-structure/age-range">

                <fo:list-item>
                    <fo:list-item-label width="0.2cm">
                        <fo:block>-</fo:block>
                    </fo:list-item-label>
                    <fo:list-item-body start-indent="8pt">
                        <fo:block>
                            <xsl:value-of select="@from" />
                            <xsl:choose>
                                <xsl:when test="@to">
                                    - <xsl:value-of select="@to" />
                                </xsl:when>
                                <xsl:otherwise>
                                    + 
                                </xsl:otherwise>
                            </xsl:choose>
                            : <xsl:value-of select="@percentage" />%
                            
                            <fo:list-block>

                                <fo:list-item>
                                    <fo:list-item-label width="0.2cm">
                                        <fo:block>-</fo:block>
                                    </fo:list-item-label>
                                    <fo:list-item-body start-indent="16pt">
                                        <fo:block>
                                            Male: <xsl:value-of select="male/@amount" />
                                        </fo:block>
                                    </fo:list-item-body>
                                </fo:list-item>

                                <fo:list-item>
                                    <fo:list-item-label width="0.2cm">
                                        <fo:block>-</fo:block>
                                    </fo:list-item-label>
                                    <fo:list-item-body start-indent="16pt">
                                        <fo:block>
                                            Female: <xsl:value-of select="female/@amount" />
                                        </fo:block>
                                    </fo:list-item-body>
                                </fo:list-item>

                            </fo:list-block>
                        </fo:block>
                    </fo:list-item-body>
                </fo:list-item>

            </xsl:for-each>
        </fo:list-block>
    </xsl:template>
</xsl:stylesheet>