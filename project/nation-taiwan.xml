<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE nation SYSTEM "nation.dtd">

<nation name="Taiwan" id="taiwan">
	<source url="https://www.cia.gov/library/publications/resources/the-world-factbook/geos/tw.html" />
	<introduction>
		<background>First inhabited by Austronesian people, Taiwan became home to Han immigrants beginning in the late Ming Dynasty (17th century). In 1895, military defeat forced China's Qing Dynasty to cede Taiwan to Japan, which then governed Taiwan for 50 years. Taiwan came under Chinese Nationalist (Kuomintang, KMT) control after World War II. With the communist victory in the Chinese civil war in 1949, the Nationalist-controlled Republic of China government and 2 million Nationalists fled to Taiwan and continued to claim to be the legitimate government for mainland China and Taiwan based on a 1947 Constitution drawn up for all of China. Until 1987, however, the Nationalist government ruled Taiwan under a civil war martial law declaration dating to 1948. Beginning in the 1970s, Nationalist authorities gradually began to incorporate the native population into the governing structure beyond the local level. The democratization process expanded rapidly in the 1980s, leading to the then illegal founding of Taiwan’s first opposition party (the Democratic Progressive Party or DPP) in 1986 and the lifting of martial law the following year. Taiwan held legislative elections in 1992, the first in over forty years, and its first direct presidential election in 1996. In the 2000 presidential elections, Taiwan underwent its first peaceful transfer of power with the KMT loss to the DPP and afterwards experienced two additional democratic transfers of power in 2008 and 2016. Throughout this period, the island prospered, became one of East Asia's economic "Tigers," and after 2000 became a major investor in mainland China as cross-Strait ties matured. The dominant political issues continue to be economic reform and growth as well as management of sensitive relations between Taiwan and China.</background>
	</introduction>
	<geography>
		<location>Eastern Asia, islands bordering the East China Sea, Philippine Sea, South China Sea, and Taiwan Strait, north of the Philippines, off the southeastern coast of China</location>
		<geographic-coordinates>23 30 N, 121 00 E</geographic-coordinates>
		<map-references>Southeast Asia</map-references>
		<area>
			<area-total>35980 sq km</area-total>
			<land>32260 sq km</land>
			<water>3720 sq km</water>
			<country-comparison-to-the-world>139</country-comparison-to-the-world>
		</area>
		<area-comparative>slightly smaller than Maryland and Delaware combined</area-comparative>
		<land-boundaries>0 km</land-boundaries>
		<coastline>1566 km</coastline>
		<maritime-claims>
			<territorial-sea>12 nm</territorial-sea>
			<exclusive-economic-zone>200 nm</exclusive-economic-zone>
		</maritime-claims>
		<climate>tropical; marine; rainy season during southwest monsoon (June to August); persistent and extensive cloudiness all year</climate>
		<terrain>eastern two-thirds mostly rugged mountains; flat to gently rolling plains in west</terrain>
		<elevation>highest point: Yu Shan 3,952 m
			<mean-elevation>1,150 m</mean-elevation>
			<elevation-extremes>lowest point: South China Sea 0 m</elevation-extremes>
		</elevation>
		<natural-resources>
			<natural-resource>small deposits of coal</natural-resource>
			<natural-resource>natural gas</natural-resource>
			<natural-resource>limestone</natural-resource>
			<natural-resource>marble</natural-resource>
			<natural-resource>asbestos</natural-resource>
			<natural-resource>arable land</natural-resource>
		</natural-resources>
		<land-use>
			<land-use-category type="Agricultural land" percentage="22.7" />
			<land-use-category type="Forest" percentage="0" />
			<land-use-category type="Other" percentage="77.3" />
		</land-use>
		<irrigated-land>3,820 sq km (2012)</irrigated-land>
		<population-distribution>distribution exhibits a peripheral coastal settlement pattern, with the largest populations on the north and west coasts</population-distribution>
		<natural-hazards>earthquakes; typhoons</natural-hazards>
		<environment-current-issues>air pollution; water pollution from industrial emissions, raw sewage; contamination of drinking water supplies; trade in endangered species; low-level radioactive waste disposal</environment-current-issues>
		<geography-note>strategic location adjacent to both the Taiwan Strait and the Luzon Strait</geography-note>
	</geography>
	<people-and-society>
		<population>23,508,428 (July 2017 est.)
			<country-comparison-to-the-world>55</country-comparison-to-the-world>
		</population>
		<nationality>
			<noun>Taiwan (singular and plural)</noun>
			<adjective>Taiwan (or Taiwanese)</adjective>
		</nationality>
		<ethnic-groups>
			<ethnic-group name="Han Chinese" percentage="95" majority="true" />
			<ethnic-group name="Malayo-Polynesian" percentage="2.3" majority="false" />
		</ethnic-groups>
		<languages>
			<language name="Mandarin Chinese" official="true" />
			<language name="Taiwanese" official="false" />
			<language name="Hakka dialects" official="false" />
		</languages>
		<religions>
			<religion name="Buddhist" percentage="35.3" />
			<religion name="Taoist" percentage="33.2" />
			<religion name="Christian" percentage="3.9" />
			<religion name="Taoist or Confucian folk religionist" percentage="10" />
			<religion name="None or unspecified" percentage="18.2" />
		</religions>
		<age>
			<median total="40.7" male="40" female="41.5" />
			<age-structure>
				<age-range from="0" to="14" percentage="12.88">
					<male amount="1559074" />
					<female amount="1468319" />
				</age-range>
				<age-range from="15" to="24" percentage="12.88">
					<male amount="1551228" />
					<female amount="1476660" />
				</age-range>
				<age-range from="25" to="54" percentage="46.41">
					<male amount="5445338" />
					<female amount="5463804" />
				</age-range>
				<age-range from="55" to="64" percentage="14.12">
					<male amount="1622111" />
					<female amount="1696564" />
				</age-range>
				<age-range from="65" percentage="13.72">
					<male amount="1475534" />
					<female amount="1749796" />
				</age-range>
			</age-structure>
		</age>
		<growth-rate percentage="0.17" />
		<birth-rate amount="8.3" unit="births/1,000 population" />
		<death-rate amount="7.4" unit="deaths/1,000 population" />
		<migration-rate amount="0.9" unit="migrant(s)/1,000 population" />
		<major-urban-areas-population>TAIPEI (capital) 2.666 million; Kaohsiung 1.523 million; Taichung 1.225 million; Tainan 815,000 (2015)</major-urban-areas-population>
		<sex-ratios>
			<sex-ratio type="At birth" amount="1.07 male(s)/female" />
            <sex-ratio type="0-14 years" amount="1.08 male(s)/female" />
            <sex-ratio type="15-24 years" amount="1 male(s)/female" />
            <sex-ratio type="25-54 years" amount="0.96 male(s)/female" />
            <sex-ratio type="55-64 years" amount="0.89 male(s)/female" />
            <sex-ratio type="65+ years" amount="0.86 male(s)/female" />
            <sex-ratio type="Total" amount="0.99 male(s)/female" />
		</sex-ratios>
		<infant-mortality-rate>
			<infant-mortality-rate-total>4.3 deaths/1,000 live births</infant-mortality-rate-total>
			<infant-mortality-rate-male>4.7 deaths/1,000 live births</infant-mortality-rate-male>
			<infant-mortality-rate-female>3.9 deaths/1,000 live births (2017 est.)</infant-mortality-rate-female>
			<country-comparison-to-the-world>187</country-comparison-to-the-world>
		</infant-mortality-rate>
		<life-expectancy-at-birth>
			<total-population>80.2 years</total-population>
			<expectancy-male>77.1 years</expectancy-male>
			<expectancy-female>83.6 years (2017 est.)</expectancy-female>
			<country-comparison-to-the-world>40</country-comparison-to-the-world>
		</life-expectancy-at-birth>
		<total-fertility-rate>1.13 children born/woman (2017 est.)
			<country-comparison-to-the-world>222</country-comparison-to-the-world>
		</total-fertility-rate>
	</people-and-society>
	<government>
		<country-name>
			<conventional-long-form>none</conventional-long-form>
			<conventional-short-form>Taiwan</conventional-short-form>
			<local-long-form>none</local-long-form>
			<local-short-form>Taiwan</local-short-form>
			<etymology>"Tayowan" was the name of the coastal sandbank where the Dutch erected their colonial headquarters on the island in the 17th century; the former name "Formosa" means "beautiful" in Portuguese</etymology>
		</country-name>
		<government-type>semi-presidential republic</government-type>
		<capital>
			<capital-name>Taipei</capital-name>
			<geographic-coordinates>25 02 N, 121 31 E</geographic-coordinates>
			<time-difference>UTC+8 (13 hours ahead of Washington, DC, during Standard Time)</time-difference>
		</capital>
		<administrative-divisions>includes main island of Taiwan plus smaller islands nearby and off coast of China's Fujian Province; Taiwan is divided into 13 counties (xian, singular and plural), 3 cities (shi, singular and plural), and 6 special municipalities directly under the jurisdiction of the Executive Yuan</administrative-divisions>
		<national-holiday>Republic Day (National Day), 10 October (1911); note - celebrates the anniversary of the Chinese Revolution, also known as Double Ten (10-10) Day</national-holiday>
		<constitution>previous 1912, 1931; latest adopted 25 December 1946, promulgated 1 January 1947, effective 25 December 1947; amended/revised several times, last in 2005 (2016)</constitution>
		<legal-system>civil law system</legal-system>
		<suffrage>20 years of age; universal; note - in mid-2016, the Legislative Yuan drafted a constitutional amendment to reduce the voting age to 18, but it has not passed as of December 2017</suffrage>
		<executive-branch>
			<chief-of-state>President TSAI Ing-wen (since 20 May 2016); Vice President CHEN Chien-jen (since 20 May 2016)</chief-of-state>
			<head-of-government>Premier LAI Ching-te (President of the Executive Yuan) (since 8 September 2017); Vice Premier LIN Hsi-yao, Vice President of the Executive Yuan (since 20 May 2016)</head-of-government>
			<cabinet>Executive Yuan - ministers appointed by president on recommendation of premier</cabinet>
		</executive-branch>
		<legislative-branch>
			<description>unicameral Legislative Yuan (113 seats; 73 members directly elected in single-seat constituencies by simple majority vote, 34 directly elected in a single island-wide constituency by proportional representation vote, and 6 directly elected in multi-seat aboriginal constituencies by proportional representation vote; members serve 4-year terms)</description>
			<elections>last held on 16 January 2016 (next to be held in January 2020)</elections>
			<election-results>percent of vote by party - NA; seats by party - DPP 68, KMT 35, NPP 5, PFP 3, NPSU 1, independent 1; note - this is the first non-KMT-led legislature in Taiwan's history</election-results>
		</legislative-branch>
		<judicial-branch>
			<highest-court>Supreme Court (consists of the court president, vice president, and approximately 100 judges organized into 8 civil and 12 criminal divisions, each with a division chief justice and 4 associate justices); Constitutional Court (consists of the court president, vice president, and 13 justices)</highest-court>
			<judge-selection-and-term-of-office>Supreme Court justices appointed by the president; Constitutional Court justices appointed by the president with approval of the Legislative Yuan; Supreme Court justices appointed for life; Constitutional Court justices appointed for 8-year terms with half the membership renewed every 4 years</judge-selection-and-term-of-office>
			<subordinate-courts>high courts; district courts; hierarchy of administrative courts</subordinate-courts>
		</judicial-branch>
		<political-parties-and-leaders>People First Party or PFP [James SOONG Chu-yu]</political-parties-and-leaders>
		<flag-description>red field with a dark blue rectangle in the upper hoist-side corner bearing a white sun with 12 triangular rays; the blue and white design of the canton (symbolizing the sun of progress) dates to 1895; it was later adopted as the flag of the Kuomintang Party; blue signifies liberty, justice, and democracy; red stands for fraternity, sacrifice, and nationalism, white represents equality, frankness, and the people's livelihood; the 12 rays of the sun are those of the months and the twelve traditional Chinese hours (each ray equals two hours)</flag-description>
		<national-symbol>white, 12-rayed sun on blue field; national colors: blue, white, red</national-symbol>
		<national-anthem>
			<anthem-name>"Zhonghua Minguo guoge" (National Anthem of the Republic of China)</anthem-name>
			<music>HU Han-min, TAI Chi-t'ao, and LIAO Chung-k'ai/CHENG Mao-Yun</music>
			<anthem-note>adopted 1930; also the song of the Kuomintang Party; it is informally known as "San Min Chu I" or "San Min Zhu Yi" (Three Principles of the People); because of political pressure from China, "Guo Qi Ge" (National Banner Song) is used at international events rather than the official anthem of Taiwan; the "National Banner Song" has gained popularity in Taiwan and is commonly used during flag raisings</anthem-note>
		</national-anthem>
	</government>
	<economy>
		<economy-overview>Closer economic links with the mainland bring opportunities for Taiwan’s economy but also pose challenges as political differences remain unresolved and China’s economic growth is slowing. Domestic economic issues loomed large in public debate ahead of the January 2016 presidential and legislative elections, including concerns about stagnant wages, high housing prices, youth unemployment, job security, and financial security in retirement.</economy-overview>
		<gdp-purchasing-power-parity>$1.136 trillion (2015 est.)
			<country-comparison-to-the-world>23</country-comparison-to-the-world>
		</gdp-purchasing-power-parity>
		<gdp-official-exchange-rate>$571.5 billion (2016 est.)</gdp-official-exchange-rate>
		<gdp-real-growth-rate>0.7% (2015 est.)
			<country-comparison-to-the-world>156</country-comparison-to-the-world>
		</gdp-real-growth-rate>
		<gdp-per-capita>$48,300 (2015 est.)
			<country-comparison-to-the-world>30</country-comparison-to-the-world>
		</gdp-per-capita>
		<by-end-use>
			<household-consumption>52.4%</household-consumption>
			<government-consumption>14.1%</government-consumption>
			<investment-in-fixed-capital>21.4%</investment-in-fixed-capital>
			<investment-in-inventories>0%</investment-in-inventories>
			<exports-of-goods-and-services>64.8%</exports-of-goods-and-services>
			<imports-of-goods-and-services>-52.7% (2017 est.)</imports-of-goods-and-services>
		</by-end-use>
		<by-sector-of-origin>
			<agriculture>1.8%</agriculture>
			<industry>36%</industry>
			<services>62.1% (2017 est.)</services>
		</by-sector-of-origin>
		<agriculture-products>
			<agriculture-product>rice</agriculture-product>
			<agriculture-product>vegetables</agriculture-product>
			<agriculture-product>fruit</agriculture-product>
			<agriculture-product>tea</agriculture-product>
			<agriculture-product>flowers</agriculture-product>
			<agriculture-product>pigs</agriculture-product>
			<agriculture-product>poultry</agriculture-product>
			<agriculture-product>fish</agriculture-product>
		</agriculture-products>
		<industries>
			<industry>electronics</industry>
			<industry>communications and information technology products</industry>
			<industry>petroleum refining</industry>
			<industry>chemicals</industry>
			<industry>textiles</industry>
			<industry>iron and steel</industry>
			<industry>machinery</industry>
			<industry>cement</industry>
			<industry>food processing</industry>
			<industry>vehicles</industry>
			<industry>consumer products</industry>
			<industry>pharmaceuticals</industry>
		</industries>
		<industrial-production-growth-rate>2% (2017 est.)</industrial-production-growth-rate>
		<labor-force>11.78 million (2017 est.)
			<country-comparison-to-the-world>52</country-comparison-to-the-world>
		</labor-force>
		<labor-force-by-occupation>
			<agriculture>4.9%</agriculture>
			<industry>35.9%</industry>
			<services>59.2% (2016 est.)</services>
		</labor-force-by-occupation>
		<unemployment-rate>3.9% (2016 est.)
			<country-comparison-to-the-world>43</country-comparison-to-the-world>
		</unemployment-rate>
		<taxes-and-other-revenues>16.3% of GDP (2017 est.)
			<country-comparison-to-the-world>182</country-comparison-to-the-world>
		</taxes-and-other-revenues>
		<public-debt>31.2% of GDP (2016 est.)
			<country-comparison-to-the-world>163</country-comparison-to-the-world>
		</public-debt>
		<inflation-rate-consumer-prices>1.4% (2016 est.)
			<country-comparison-to-the-world>39</country-comparison-to-the-world>
		</inflation-rate-consumer-prices>
		<exports>$310.4 billion (2016 est.)
			<country-comparison-to-the-world>15</country-comparison-to-the-world>
		</exports>
		<exports-commodities>semiconductors, petrochemicals, automobile/auto parts, ships, wireless communication equipment, flat display displays, steel, electronics, plastics, computers</exports-commodities>
		<imports>$239.7 billion (2016 est.)
			<country-comparison-to-the-world>19</country-comparison-to-the-world>
		</imports>
		<imports-commodities>oil/petroleum, semiconductors, natural gas, coal, steel, computers, wireless communication equipment, automobiles, fine chemicals, textiles</imports-commodities>
		<exchange-rates>30.363 (2013 est.)</exchange-rates>
	</economy>
	<energy>
		<electricity-production>264.1 billion kWh (2016 est.)
			<country-comparison-to-the-world>18</country-comparison-to-the-world>
		</electricity-production>
		<electricity-consumption>255.3 billion kWh (2016 est.)
			<country-comparison-to-the-world>15</country-comparison-to-the-world>
		</electricity-consumption>
		<electricity-exports>0 kWh (2016 est.)
			<country-comparison-to-the-world>208</country-comparison-to-the-world>
		</electricity-exports>
		<electricity-imports>0 kWh (2016 est.)
			<country-comparison-to-the-world>212</country-comparison-to-the-world>
		</electricity-imports>
		<crude-oil-proved-reserves>2.38 million bbl (1 January 2017 es)
			<country-comparison-to-the-world>97</country-comparison-to-the-world>
		</crude-oil-proved-reserves>
		<refined-petroleum-products-production>917,500 bbl/day (2014 est.)
			<country-comparison-to-the-world>24</country-comparison-to-the-world>
		</refined-petroleum-products-production>
		<refined-petroleum-products-consumption>955,300 bbl/day (2015 est.)
			<country-comparison-to-the-world>23</country-comparison-to-the-world>
		</refined-petroleum-products-consumption>
		<refined-petroleum-products-exports>350,300 bbl/day (2014 est.)
			<country-comparison-to-the-world>24</country-comparison-to-the-world>
		</refined-petroleum-products-exports>
		<refined-petroleum-products-imports>416,200 bbl/day (2014 est.)
			<country-comparison-to-the-world>21</country-comparison-to-the-world>
		</refined-petroleum-products-imports>
		<natural-gas-production>340 million cu m (2015 est.)
			<country-comparison-to-the-world>75</country-comparison-to-the-world>
		</natural-gas-production>
		<natural-gas-consumption>31.71 billion cu m (2015 est.)
			<country-comparison-to-the-world>37</country-comparison-to-the-world>
		</natural-gas-consumption>
		<natural-gas-exports>0 cu m (2016 est.)
			<country-comparison-to-the-world>198</country-comparison-to-the-world>
		</natural-gas-exports>
		<natural-gas-imports>19.39 billion cu m (2015 est.)
			<country-comparison-to-the-world>16</country-comparison-to-the-world>
		</natural-gas-imports>
		<natural-gas-proved-reserves>6.229 billion cu m (1 January 2017 es)
			<country-comparison-to-the-world>91</country-comparison-to-the-world>
		</natural-gas-proved-reserves>
	</energy>
	<communications>
		<telephone-system>
			<general-assessment>provides telecommunications service for every business and private need</general-assessment>
			<domestic>thoroughly modern; completely digitalized</domestic>
			<international>country code - 886; roughly 15 submarine fiber cables provide links throughout Asia, Australia, the Middle East, Europe, and the US; satellite earth stations - 2 (2016)</international>
		</telephone-system>
		<broadcast-media>5 nationwide television networks operating roughly 75 TV stations; about 60% of households utilize multi-channel cable TV; national and regional radio networks with about 170 radio stations (2016)</broadcast-media>
		<internet-country-code>.tw</internet-country-code>
		<internet-users>
			<total-users>20.601 million</total-users>
			<percent-of-population>88% (July 2016 est.)</percent-of-population>
			<country-comparison-to-the-world>36</country-comparison-to-the-world>
		</internet-users>
	</communications>
	<transportation>
		<national-air-transport-system>
			<number-of-registered-air-carriers>8</number-of-registered-air-carriers>
			<inventory-of-registered-aircraft-operated-by-air-carriers>221 (2015)</inventory-of-registered-aircraft-operated-by-air-carriers>
		</national-air-transport-system>
		<civil-aircraft-registration-country-code-prefix>B (2016)</civil-aircraft-registration-country-code-prefix>
		<airports>37 (2013)
			<country-comparison-to-the-world>107</country-comparison-to-the-world>
		</airports>
		<airports-with-paved-runways>
			<airport type="Total" amount="35" />
			<airport type="47m" amount="15" />
			<airport type="437m" amount="10" />
			<airport type="523m" amount="8" />
			<airport type="Under 914" amount="2" />
		</airports-with-paved-runways>
		<railways>
			<railways-total>1,597 km</railways-total>
			<standard-gauge>345 km 1.435-m gauge (345 km electrified)</standard-gauge>
			<narrow-gauge>1,102 km 1.067-m gauge (692 km electrified); 150 km 0.762-m gauge</narrow-gauge>
			<country-comparison-to-the-world>79</country-comparison-to-the-world>
		</railways>
		<roadways>
			<roadways-total>42,520 km</roadways-total>
			<paved>42,078 km (includes 1,348 km of highways and 737 km of expressways)</paved>
			<unpaved>442 km (2013)</unpaved>
			<country-comparison-to-the-world>85</country-comparison-to-the-world>
		</roadways>
		<merchant-marine>
			<merchant-total>350</merchant-total>
			<by-type>bulk carrier 26, container ship 38, general cargo 59, oil tanker 24, other 203 (2017)</by-type>
			<country-comparison-to-the-world>49</country-comparison-to-the-world>
		</merchant-marine>
		<ports-and-terminals>
			<major-seaport>Keelung (Chi-lung), Kaohsiung, Hualian, Taichung</major-seaport>
			<lng-terminal>Yung An (Kaohsiung), Taichung</lng-terminal>
		</ports-and-terminals>
	</transportation>
	<military-and-security>
		<military-branches>Army, Navy (includes Marine Corps), Air Force, Military Police Command, Armed Forces Reserve Command, Coast Guard Administration (2016)</military-branches>
		<military-service-age-and-obligation>starting with those born in 1994, males 18-36 years of age may volunteer for military service or must complete 4 months of compulsory military training (or substitute civil service in some cases); men born before December 1993 are required to complete compulsory service for 1 year (military or civil); men are subject to training recalls up to four times for periods not to exceed 20 days for 8 years after discharge; women may enlist, but are restricted to noncombat roles in most cases; Taiwan is planning to implement an all-volunteer military in 2018 (2017) (2016)</military-service-age-and-obligation>
	</military-and-security>
</nation>