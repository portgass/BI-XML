<!ELEMENT nation (source, introduction, geography, people-and-society, government, economy, energy, communications, transportation, military-and-security)>
<!ATTLIST nation name CDATA #REQUIRED>
<!ATTLIST nation id CDATA #REQUIRED>

<!ELEMENT source EMPTY>
<!ATTLIST source url CDATA #REQUIRED>

<!ELEMENT introduction (background)>
<!ELEMENT background (#PCDATA)>

<!ELEMENT geography (
    location,
    geographic-coordinates,
    map-references,
    area,
    area-comparative,
    land-boundaries,
    coastline,
    maritime-claims,
    climate,
    terrain,
    elevation,
    natural-resources,
    land-use,
    irrigated-land,
    population-distribution,
    natural-hazards,
    environment-current-issues,
    geography-note
)>

<!ELEMENT location (#PCDATA)>

<!ELEMENT geographic-coordinates (#PCDATA)>

<!ELEMENT map-references (#PCDATA)>

<!ELEMENT area (area-total, land, water, country-comparison-to-the-world)>
<!ELEMENT area-total (#PCDATA)>
<!ELEMENT land (#PCDATA)>
<!ELEMENT water (#PCDATA)>
<!ELEMENT country-comparison-to-the-world (#PCDATA)>

<!ELEMENT area-comparative (#PCDATA)>

<!ELEMENT land-boundaries (#PCDATA)>

<!ELEMENT coastline (#PCDATA)>

<!ELEMENT maritime-claims (territorial-sea, exclusive-economic-zone?, continental-shelf?)>
<!ELEMENT territorial-sea (#PCDATA)>
<!ELEMENT exclusive-economic-zone (#PCDATA)>
<!ELEMENT continental-shelf (#PCDATA)>

<!ELEMENT climate (#PCDATA)>

<!ELEMENT terrain (#PCDATA)>

<!ELEMENT elevation (#PCDATA | mean-elevation | elevation-extremes)*>
<!ELEMENT mean-elevation (#PCDATA)>
<!ELEMENT elevation-extremes (#PCDATA)>

<!ELEMENT natural-resources (natural-resource+)>
<!ELEMENT natural-resource (#PCDATA)>

<!ELEMENT land-use (land-use-category+)>
<!ELEMENT land-use-category EMPTY>
<!ATTLIST land-use-category type CDATA #REQUIRED>
<!ATTLIST land-use-category percentage CDATA #REQUIRED>

<!ELEMENT irrigated-land (#PCDATA)>

<!ELEMENT population-distribution (#PCDATA)>

<!ELEMENT natural-hazards (#PCDATA)>

<!ELEMENT environment-current-issues (#PCDATA)>

<!ELEMENT geography-note (#PCDATA)>



<!ELEMENT people-and-society (
    population,
    nationality,
    ethnic-groups,
    languages,
    religions,
    age,
    growth-rate,
    birth-rate,
    death-rate,
    migration-rate,
    major-urban-areas-population,
    sex-ratios,
    infant-mortality-rate,
    life-expectancy-at-birth,
    total-fertility-rate
)>

<!ELEMENT population (#PCDATA | country-comparison-to-the-world)*>

<!ELEMENT nationality (noun, adjective)>
<!ELEMENT noun (#PCDATA)>
<!ELEMENT adjective (#PCDATA)>

<!ELEMENT ethnic-groups (ethnic-group+)>
<!ELEMENT ethnic-group EMPTY>
<!ATTLIST ethnic-group name CDATA #REQUIRED>
<!ATTLIST ethnic-group percentage CDATA #IMPLIED>
<!ATTLIST ethnic-group majority (true|false) #REQUIRED>

<!ELEMENT languages (language+)>

<!ELEMENT language EMPTY>
<!ATTLIST language name CDATA #REQUIRED>
<!ATTLIST language percentage CDATA #IMPLIED>
<!ATTLIST language official (true|false) #REQUIRED>

<!ELEMENT religions (religion+)>

<!ELEMENT religion (branch*)>
<!ATTLIST religion name CDATA #REQUIRED>
<!ATTLIST religion percentage CDATA #REQUIRED>
<!ATTLIST religion official (true|false) #IMPLIED>

<!ELEMENT branch EMPTY>
<!ATTLIST branch name CDATA #REQUIRED>
<!ATTLIST branch percentage CDATA #REQUIRED>

<!ELEMENT age (median, age-structure)>

<!ELEMENT median EMPTY>
<!ATTLIST median total CDATA #REQUIRED>
<!ATTLIST median male CDATA #REQUIRED>
<!ATTLIST median female CDATA #REQUIRED>

<!ELEMENT age-structure (age-range+)>

<!ELEMENT age-range (male, female)>
<!ATTLIST age-range from CDATA #REQUIRED>
<!ATTLIST age-range to CDATA #IMPLIED>
<!ATTLIST age-range percentage CDATA #REQUIRED>

<!ELEMENT male EMPTY>
<!ATTLIST male amount CDATA #REQUIRED>

<!ELEMENT female EMPTY>
<!ATTLIST female amount CDATA #REQUIRED>

<!ELEMENT growth-rate EMPTY>
<!ATTLIST growth-rate percentage CDATA #REQUIRED>

<!ELEMENT birth-rate EMPTY>
<!ATTLIST birth-rate amount CDATA #REQUIRED>
<!ATTLIST birth-rate unit CDATA #REQUIRED>

<!ELEMENT death-rate EMPTY>
<!ATTLIST death-rate amount CDATA #REQUIRED>
<!ATTLIST death-rate unit CDATA #REQUIRED>

<!ELEMENT migration-rate EMPTY>
<!ATTLIST migration-rate amount CDATA #REQUIRED>
<!ATTLIST migration-rate unit CDATA #REQUIRED>

<!ELEMENT major-urban-areas-population (#PCDATA)>

<!ELEMENT sex-ratios (sex-ratio+)>

<!ELEMENT sex-ratio EMPTY>
<!ATTLIST sex-ratio type CDATA #REQUIRED>
<!ATTLIST sex-ratio amount CDATA #REQUIRED>

<!ELEMENT infant-mortality-rate (infant-mortality-rate-total, infant-mortality-rate-male, infant-mortality-rate-female, country-comparison-to-the-world)>
<!ELEMENT infant-mortality-rate-total (#PCDATA)>
<!ELEMENT infant-mortality-rate-male (#PCDATA)>
<!ELEMENT infant-mortality-rate-female (#PCDATA)>

<!ELEMENT life-expectancy-at-birth (total-population, expectancy-male, expectancy-female, country-comparison-to-the-world)>
<!ELEMENT total-population (#PCDATA)>
<!ELEMENT expectancy-male (#PCDATA)>
<!ELEMENT expectancy-female (#PCDATA)>

<!ELEMENT total-fertility-rate (#PCDATA | country-comparison-to-the-world)*>



<!ELEMENT government (
    country-name,
    government-type,
    capital,
    administrative-divisions,
    national-holiday,
    constitution,
    legal-system,
    suffrage,
    executive-branch,
    legislative-branch,
    judicial-branch,
    political-parties-and-leaders,
    flag-description,
    national-symbol,
    national-anthem
)>

<!ELEMENT country-name (conventional-long-form, conventional-short-form, local-long-form?, local-short-form?, abbreviation?, etymology)>
<!ELEMENT conventional-long-form (#PCDATA)>
<!ELEMENT conventional-short-form (#PCDATA)>
<!ELEMENT local-long-form (#PCDATA)>
<!ELEMENT local-short-form (#PCDATA)>
<!ELEMENT abbreviation (#PCDATA)>
<!ELEMENT etymology (#PCDATA)>

<!ELEMENT government-type (#PCDATA)>

<!ELEMENT capital (capital-name, geographic-coordinates, time-difference)>
<!ELEMENT capital-name (#PCDATA)>
<!ELEMENT time-difference (#PCDATA)>

<!ELEMENT administrative-divisions (#PCDATA)>

<!ELEMENT national-holiday (#PCDATA)>

<!ELEMENT constitution (#PCDATA)>

<!ELEMENT legal-system (#PCDATA)>

<!ELEMENT suffrage (#PCDATA)>

<!ELEMENT executive-branch (chief-of-state, head-of-government, cabinet)>
<!ELEMENT chief-of-state (#PCDATA)>
<!ELEMENT head-of-government (#PCDATA)>
<!ELEMENT cabinet (#PCDATA)>

<!ELEMENT legislative-branch (description, elections, election-results)>
<!ELEMENT description (#PCDATA)>
<!ELEMENT elections (#PCDATA)>
<!ELEMENT election-results (#PCDATA)>

<!ELEMENT judicial-branch (highest-court, judge-selection-and-term-of-office, subordinate-courts)>
<!ELEMENT highest-court (#PCDATA)>
<!ELEMENT judge-selection-and-term-of-office (#PCDATA)>
<!ELEMENT subordinate-courts (#PCDATA)>

<!ELEMENT political-parties-and-leaders (#PCDATA)>

<!ELEMENT flag-description (#PCDATA)>

<!ELEMENT national-symbol (#PCDATA)>

<!ELEMENT national-anthem (anthem-name, music, anthem-note)>
<!ELEMENT anthem-name (#PCDATA)>
<!ELEMENT music (#PCDATA)>
<!ELEMENT anthem-note (#PCDATA)>



<!ELEMENT economy (
    economy-overview,
    gdp-purchasing-power-parity,
    gdp-official-exchange-rate,
    gdp-real-growth-rate,
    gdp-per-capita,
    by-end-use,
    by-sector-of-origin,
    agriculture-products,
    industries,
    industrial-production-growth-rate,
    labor-force,
    labor-force-by-occupation,
    unemployment-rate,
    taxes-and-other-revenues,
    public-debt,
    inflation-rate-consumer-prices,
    exports,
    exports-commodities,
    imports,
    imports-commodities,
    exchange-rates
)>

<!ELEMENT economy-overview (#PCDATA)>

<!ELEMENT gdp-purchasing-power-parity (#PCDATA | country-comparison-to-the-world)*>

<!ELEMENT gdp-official-exchange-rate (#PCDATA)>

<!ELEMENT gdp-real-growth-rate (#PCDATA | country-comparison-to-the-world)*>

<!ELEMENT gdp-per-capita (#PCDATA | country-comparison-to-the-world)*>

<!ELEMENT by-end-use (household-consumption, government-consumption, investment-in-fixed-capital, investment-in-inventories, exports-of-goods-and-services,imports-of-goods-and-services)>
<!ELEMENT household-consumption (#PCDATA)>
<!ELEMENT government-consumption (#PCDATA)>
<!ELEMENT investment-in-fixed-capital (#PCDATA)>
<!ELEMENT investment-in-inventories (#PCDATA)>
<!ELEMENT exports-of-goods-and-services (#PCDATA)>
<!ELEMENT imports-of-goods-and-services (#PCDATA)>

<!ELEMENT by-sector-of-origin (agriculture, industry, services)>
<!ELEMENT agriculture (#PCDATA)>
<!ELEMENT industry (#PCDATA)>
<!ELEMENT services (#PCDATA)>

<!ELEMENT agriculture-products (agriculture-product+)>
<!ELEMENT agriculture-product (#PCDATA)>

<!ELEMENT industries (industry+)>

<!ELEMENT industrial-production-growth-rate (#PCDATA)>

<!ELEMENT labor-force (#PCDATA | country-comparison-to-the-world)*>

<!ELEMENT labor-force-by-occupation (agriculture, industry, services)>

<!ELEMENT unemployment-rate (#PCDATA | country-comparison-to-the-world)*>

<!ELEMENT taxes-and-other-revenues (#PCDATA | country-comparison-to-the-world)*>

<!ELEMENT public-debt (#PCDATA | country-comparison-to-the-world)*>

<!ELEMENT inflation-rate-consumer-prices (#PCDATA | country-comparison-to-the-world)*>

<!ELEMENT exports (#PCDATA | country-comparison-to-the-world)*>

<!ELEMENT exports-commodities (#PCDATA)>

<!ELEMENT imports (#PCDATA | country-comparison-to-the-world)*>

<!ELEMENT imports-commodities (#PCDATA)>

<!ELEMENT exchange-rates (#PCDATA)>



<!ELEMENT energy (
    electricity-production,
    electricity-consumption,
    electricity-exports,
    electricity-imports,
    crude-oil-proved-reserves,
    refined-petroleum-products-production,
    refined-petroleum-products-consumption,
    refined-petroleum-products-exports,
    refined-petroleum-products-imports,
    natural-gas-production,
    natural-gas-consumption,
    natural-gas-exports,
    natural-gas-imports,
    natural-gas-proved-reserves
)>

<!ELEMENT electricity-production (#PCDATA | country-comparison-to-the-world)*>

<!ELEMENT electricity-consumption (#PCDATA | country-comparison-to-the-world)*>

<!ELEMENT electricity-exports (#PCDATA | country-comparison-to-the-world)*>

<!ELEMENT electricity-imports (#PCDATA | country-comparison-to-the-world)*>

<!ELEMENT crude-oil-proved-reserves (#PCDATA | country-comparison-to-the-world)*>

<!ELEMENT refined-petroleum-products-production (#PCDATA | country-comparison-to-the-world)*>

<!ELEMENT refined-petroleum-products-consumption (#PCDATA | country-comparison-to-the-world)*>

<!ELEMENT refined-petroleum-products-exports (#PCDATA | country-comparison-to-the-world)*>

<!ELEMENT refined-petroleum-products-imports (#PCDATA | country-comparison-to-the-world)*>

<!ELEMENT natural-gas-production (#PCDATA | country-comparison-to-the-world)*>

<!ELEMENT natural-gas-consumption (#PCDATA | country-comparison-to-the-world)*>

<!ELEMENT natural-gas-exports (#PCDATA | country-comparison-to-the-world)*>

<!ELEMENT natural-gas-imports (#PCDATA | country-comparison-to-the-world)*>

<!ELEMENT natural-gas-proved-reserves (#PCDATA | country-comparison-to-the-world)*>



<!ELEMENT communications (
    telephone-system,
    broadcast-media,
    internet-country-code,
    internet-users
)>

<!ELEMENT telephone-system (#PCDATA | general-assessment | domestic | international)*>
<!ELEMENT general-assessment (#PCDATA)>
<!ELEMENT domestic (#PCDATA)>
<!ELEMENT international (#PCDATA)>

<!ELEMENT broadcast-media (#PCDATA)>

<!ELEMENT internet-country-code (#PCDATA)>

<!ELEMENT internet-users (total-users, percent-of-population, country-comparison-to-the-world?)>
<!ELEMENT total-users (#PCDATA)>
<!ELEMENT percent-of-population (#PCDATA)>



<!ELEMENT transportation (
    national-air-transport-system,
    civil-aircraft-registration-country-code-prefix,
    airports,
    airports-with-paved-runways,
    railways?,
    roadways,
    merchant-marine,
    ports-and-terminals
)>

<!ELEMENT national-air-transport-system (number-of-registered-air-carriers, inventory-of-registered-aircraft-operated-by-air-carriers)>
<!ELEMENT number-of-registered-air-carriers (#PCDATA)>
<!ELEMENT inventory-of-registered-aircraft-operated-by-air-carriers (#PCDATA)>

<!ELEMENT civil-aircraft-registration-country-code-prefix (#PCDATA)>

<!ELEMENT airports (#PCDATA | country-comparison-to-the-world)*>

<!ELEMENT airports-with-paved-runways (airport*)>
<!ELEMENT airport EMPTY>
<!ATTLIST airport type CDATA #REQUIRED>
<!ATTLIST airport amount CDATA #REQUIRED>

<!ELEMENT railways (railways-total, standard-gauge?, narrow-gauge?, country-comparison-to-the-world)>
<!ELEMENT railways-total (#PCDATA)>
<!ELEMENT standard-gauge (#PCDATA)>
<!ELEMENT narrow-gauge (#PCDATA)>

<!ELEMENT roadways (roadways-total, paved?, unpaved?, country-comparison-to-the-world)>
<!ELEMENT roadways-total (#PCDATA)>
<!ELEMENT paved (#PCDATA)>
<!ELEMENT unpaved (#PCDATA)>

<!ELEMENT merchant-marine (merchant-total, by-type, country-comparison-to-the-world?)>
<!ELEMENT merchant-total (#PCDATA)>
<!ELEMENT by-type (#PCDATA)>

<!ELEMENT ports-and-terminals (major-seaport?, oil-terminal?, bulk-cargo-port?, river-port?, lng-terminal?)>
<!ELEMENT major-seaport (#PCDATA)>
<!ELEMENT oil-terminal (#PCDATA)>
<!ELEMENT bulk-cargo-port (#PCDATA)>
<!ELEMENT river-port (#PCDATA)>
<!ELEMENT lng-terminal (#PCDATA)>



<!ELEMENT military-and-security (
    military-branches,
    military-service-age-and-obligation
)>

<!ELEMENT military-branches (#PCDATA)>

<!ELEMENT military-service-age-and-obligation (#PCDATA)>