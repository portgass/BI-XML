<?xml version="1.0" encoding="UTF-8"?>
<grammar xmlns="http://relaxng.org/ns/structure/1.0" datatypeLibrary="http://www.w3.org/2001/XMLSchema-datatypes">
  <define name="comparison">
    <element name="country-comparison-to-the-world">
      <data type="int"/>
    </element>
  </define>
  <define name="km-type">
    <data type="string">
      <param name="pattern">[0-9]* km</param>
    </data>
  </define>
  <define name="square-km-type">
    <data type="string">
      <param name="pattern">[0-9]* sq km</param>
    </data>
  </define>
  <define name="nm-type">
    <data type="string">
      <param name="pattern">[0-9]* nm</param>
    </data>
  </define>
  <start>
    <element name="nation">
      <attribute name="name"/>
      <attribute name="id"/>
      <element name="source">
        <attribute name="url"/>
      </element>
      <element name="introduction">
        <element name="background">
          <text/>
        </element>
      </element>
      <element name="geography">
        <element name="location">
          <text/>
        </element>
        <element name="geographic-coordinates">
          <text/>
        </element>
        <element name="map-references">
          <text/>
        </element>
        <element name="area">
          <element name="area-total">
            <ref name="square-km-type"/>
          </element>
          <element name="land">
            <ref name="square-km-type"/>
          </element>
          <element name="water">
            <ref name="square-km-type"/>
          </element>
          <ref name="comparison"/>
        </element>
        <element name="area-comparative">
          <text/>
        </element>
        <element name="land-boundaries">
          <ref name="km-type"/>
        </element>
        <element name="coastline">
          <ref name="km-type"/>
        </element>
        <element name="maritime-claims">
          <element name="territorial-sea">
            <ref name="nm-type"/>
          </element>
          <optional>
            <element name="exclusive-economic-zone">
              <ref name="nm-type"/>
            </element>
          </optional>
          <optional>
            <element name="continental-shelf">
              <text/>
            </element>
          </optional>
        </element>
        <element name="climate">
          <text/>
        </element>
        <element name="terrain">
          <text/>
        </element>
        <element name="elevation">
          <text/>
          <element name="mean-elevation">
            <text/>
          </element>
          <element name="elevation-extremes">
            <text/>
          </element>
        </element>
        <element name="natural-resources">
          <zeroOrMore>
            <element name="natural-resource">
              <text/>
            </element>
          </zeroOrMore>
        </element>
        <element name="land-use">
          <zeroOrMore>
            <element name="land-use-category">
              <attribute name="type"/>
              <attribute name="percentage">
                <data type="double"/>
              </attribute>
            </element>
          </zeroOrMore>
        </element>
        <element name="irrigated-land">
          <text/>
        </element>
        <element name="population-distribution">
          <text/>
        </element>
        <element name="natural-hazards">
          <text/>
        </element>
        <element name="environment-current-issues">
          <text/>
        </element>
        <element name="geography-note">
          <text/>
        </element>
      </element>
      <element name="people-and-society">
        <element name="population">
          <text/>
          <ref name="comparison"/>
        </element>
        <element name="nationality">
          <element name="noun">
            <text/>
          </element>
          <element name="adjective">
            <text/>
          </element>
        </element>
        <element name="ethnic-groups">
          <zeroOrMore>
            <element name="ethnic-group">
              <attribute name="name"/>
              <optional>
                <attribute name="percentage">
                  <data type="double"/>
                </attribute>
              </optional>
              <attribute name="majority">
                <choice>
                  <value>true</value>
                  <value>false</value>
                </choice>
              </attribute>
            </element>
          </zeroOrMore>
        </element>
        <element name="languages">
          <zeroOrMore>
            <element name="language">
              <attribute name="name"/>
              <optional>
                <attribute name="percentage">
                  <data type="double"/>
                </attribute>
              </optional>
              <attribute name="official">
                <choice>
                  <value>true</value>
                  <value>false</value>
                </choice>
              </attribute>
            </element>
          </zeroOrMore>
        </element>
        <element name="religions">
          <zeroOrMore>
            <element name="religion">
              <attribute name="name"/>
              <attribute name="percentage">
                <data type="double"/>
              </attribute>
              <optional>
                <attribute name="official">
                  <choice>
                    <value>true</value>
                    <value>false</value>
                  </choice>
                </attribute>
              </optional>
              <zeroOrMore>
                <element name="branch">
                  <attribute name="name"/>
                  <attribute name="percentage">
                    <data type="double"/>
                  </attribute>
                </element>
              </zeroOrMore>
            </element>
          </zeroOrMore>
        </element>
        <element name="age">
          <element name="median">
            <attribute name="total">
              <data type="double"/>
            </attribute>
            <attribute name="male">
              <data type="double"/>
            </attribute>
            <attribute name="female">
              <data type="double"/>
            </attribute>
          </element>
          <element name="age-structure">
            <zeroOrMore>
              <element name="age-range">
                <attribute name="from">
                  <data type="int"/>
                </attribute>
                <optional>
                  <attribute name="to">
                    <data type="int"/>
                  </attribute>
                </optional>
                <attribute name="percentage">
                  <data type="double"/>
                </attribute>
                <element name="male">
                  <attribute name="amount">
                    <data type="int"/>
                  </attribute>
                </element>
                <element name="female">
                  <attribute name="amount">
                    <data type="int"/>
                  </attribute>
                </element>
              </element>
            </zeroOrMore>
          </element>
        </element>
        <element name="growth-rate">
          <attribute name="percentage">
            <data type="double"/>
          </attribute>
        </element>
        <element name="birth-rate">
          <attribute name="amount">
            <data type="double"/>
          </attribute>
          <attribute name="unit"/>
        </element>
        <element name="death-rate">
          <attribute name="amount">
            <data type="double"/>
          </attribute>
          <attribute name="unit"/>
        </element>
        <element name="migration-rate">
          <attribute name="amount">
            <data type="double"/>
          </attribute>
          <attribute name="unit"/>
        </element>
        <element name="major-urban-areas-population">
          <text/>
        </element>
        <element name="sex-ratios">
          <zeroOrMore>
            <element name="sex-ratio">
              <attribute name="type"/>
              <attribute name="amount"/>
            </element>
          </zeroOrMore>
        </element>
        <element name="infant-mortality-rate">
          <element name="infant-mortality-rate-total">
            <text/>
          </element>
          <element name="infant-mortality-rate-male">
            <text/>
          </element>
          <element name="infant-mortality-rate-female">
            <text/>
          </element>
          <ref name="comparison"/>
        </element>
        <element name="life-expectancy-at-birth">
          <element name="total-population">
            <text/>
          </element>
          <element name="expectancy-male">
            <text/>
          </element>
          <element name="expectancy-female">
            <text/>
          </element>
          <ref name="comparison"/>
        </element>
        <element name="total-fertility-rate">
          <text/>
          <ref name="comparison"/>
        </element>
      </element>
      <element name="government">
        <element name="country-name">
          <element name="conventional-long-form">
            <text/>
          </element>
          <element name="conventional-short-form">
            <text/>
          </element>
          <optional>
            <element name="local-long-form">
              <text/>
            </element>
          </optional>
          <optional>
            <element name="local-short-form">
              <text/>
            </element>
          </optional>
          <optional>
            <element name="abbreviation">
              <text/>
            </element>
          </optional>
          <element name="etymology">
            <text/>
          </element>
        </element>
        <element name="government-type">
          <text/>
        </element>
        <element name="capital">
          <element name="capital-name">
            <text/>
          </element>
          <element name="geographic-coordinates">
            <text/>
          </element>
          <element name="time-difference">
            <text/>
          </element>
        </element>
        <element name="administrative-divisions">
          <text/>
        </element>
        <element name="national-holiday">
          <text/>
        </element>
        <element name="constitution">
          <text/>
        </element>
        <element name="legal-system">
          <text/>
        </element>
        <element name="suffrage">
          <text/>
        </element>
        <element name="executive-branch">
          <element name="chief-of-state">
            <text/>
          </element>
          <element name="head-of-government">
            <text/>
          </element>
          <element name="cabinet">
            <text/>
          </element>
        </element>
        <element name="legislative-branch">
          <element name="description">
            <text/>
          </element>
          <element name="elections">
            <text/>
          </element>
          <element name="election-results">
            <text/>
          </element>
        </element>
        <element name="judicial-branch">
          <element name="highest-court">
            <text/>
          </element>
          <element name="judge-selection-and-term-of-office">
            <text/>
          </element>
          <element name="subordinate-courts">
            <text/>
          </element>
        </element>
        <element name="political-parties-and-leaders">
          <text/>
        </element>
        <element name="flag-description">
          <text/>
        </element>
        <element name="national-symbol">
          <text/>
        </element>
        <element name="national-anthem">
          <element name="anthem-name">
            <text/>
          </element>
          <element name="music">
            <text/>
          </element>
          <element name="anthem-note">
            <text/>
          </element>
        </element>
      </element>
      <element name="economy">
        <element name="economy-overview">
          <text/>
        </element>
        <element name="gdp-purchasing-power-parity">
          <text/>
          <ref name="comparison"/>
        </element>
        <element name="gdp-official-exchange-rate">
          <text/>
          <optional>
            <ref name="comparison"/>
          </optional>
        </element>
        <element name="gdp-real-growth-rate">
          <text/>
          <ref name="comparison"/>
        </element>
        <element name="gdp-per-capita">
          <text/>
          <ref name="comparison"/>
        </element>
        <element name="by-end-use">
          <element name="household-consumption">
            <text/>
          </element>
          <element name="government-consumption">
            <text/>
          </element>
          <element name="investment-in-fixed-capital">
            <text/>
          </element>
          <element name="investment-in-inventories">
            <text/>
          </element>
          <element name="exports-of-goods-and-services">
            <text/>
          </element>
          <element name="imports-of-goods-and-services">
            <text/>
          </element>
        </element>
        <element name="by-sector-of-origin">
          <element name="agriculture">
            <text/>
          </element>
          <element name="industry">
            <text/>
          </element>
          <element name="services">
            <text/>
          </element>
        </element>
        <element name="agriculture-products">
          <zeroOrMore>
            <element name="agriculture-product">
              <text/>
            </element>
          </zeroOrMore>
        </element>
        <element name="industries">
          <zeroOrMore>
            <element name="industry">
              <text/>
            </element>
          </zeroOrMore>
        </element>
        <element name="industrial-production-growth-rate">
          <text/>
        </element>
        <element name="labor-force">
          <text/>
          <ref name="comparison"/>
        </element>
        <element name="labor-force-by-occupation">
          <element name="agriculture">
            <text/>
          </element>
          <element name="industry">
            <text/>
          </element>
          <element name="services">
            <text/>
          </element>
        </element>
        <element name="unemployment-rate">
          <text/>
          <ref name="comparison"/>
        </element>
        <element name="taxes-and-other-revenues">
          <text/>
          <ref name="comparison"/>
        </element>
        <element name="public-debt">
          <text/>
          <ref name="comparison"/>
        </element>
        <element name="inflation-rate-consumer-prices">
          <text/>
          <ref name="comparison"/>
        </element>
        <element name="exports">
          <text/>
          <ref name="comparison"/>
        </element>
        <element name="exports-commodities">
          <text/>
        </element>
        <element name="imports">
          <text/>
          <ref name="comparison"/>
        </element>
        <element name="imports-commodities">
          <text/>
        </element>
        <element name="exchange-rates">
          <text/>
        </element>
      </element>
      <element name="energy">
        <element name="electricity-production">
          <text/>
          <ref name="comparison"/>
        </element>
        <element name="electricity-consumption">
          <text/>
          <ref name="comparison"/>
        </element>
        <element name="electricity-exports">
          <text/>
          <ref name="comparison"/>
        </element>
        <element name="electricity-imports">
          <text/>
          <ref name="comparison"/>
        </element>
        <element name="crude-oil-proved-reserves">
          <text/>
          <ref name="comparison"/>
        </element>
        <element name="refined-petroleum-products-production">
          <text/>
          <ref name="comparison"/>
        </element>
        <element name="refined-petroleum-products-consumption">
          <text/>
          <ref name="comparison"/>
        </element>
        <element name="refined-petroleum-products-exports">
          <text/>
          <ref name="comparison"/>
        </element>
        <element name="refined-petroleum-products-imports">
          <text/>
          <ref name="comparison"/>
        </element>
        <element name="natural-gas-production">
          <text/>
          <ref name="comparison"/>
        </element>
        <element name="natural-gas-consumption">
          <text/>
          <ref name="comparison"/>
        </element>
        <element name="natural-gas-exports">
          <text/>
          <ref name="comparison"/>
        </element>
        <element name="natural-gas-imports">
          <text/>
          <ref name="comparison"/>
        </element>
        <element name="natural-gas-proved-reserves">
          <text/>
          <ref name="comparison"/>
        </element>
      </element>
      <element name="communications">
        <element name="telephone-system">
          <choice>
            <text/>
            <group>
              <element name="general-assessment">
                <text/>
              </element>
              <element name="domestic">
                <text/>
              </element>
              <element name="international">
                <text/>
              </element>
            </group>
          </choice>
        </element>
        <element name="broadcast-media">
          <text/>
        </element>
        <element name="internet-country-code">
          <text/>
        </element>
        <element name="internet-users">
          <element name="total-users">
            <text/>
          </element>
          <element name="percent-of-population">
            <text/>
          </element>
          <optional>
            <ref name="comparison"/>
          </optional>
        </element>
      </element>
      <element name="transportation">
        <element name="national-air-transport-system">
          <element name="number-of-registered-air-carriers">
            <text/>
          </element>
          <element name="inventory-of-registered-aircraft-operated-by-air-carriers">
            <text/>
          </element>
        </element>
        <element name="civil-aircraft-registration-country-code-prefix">
          <text/>
        </element>
        <element name="airports">
          <text/>
          <optional>
            <ref name="comparison"/>
          </optional>
        </element>
        <element name="airports-with-paved-runways">
          <zeroOrMore>
            <element name="airport">
              <attribute name="type"/>
              <attribute name="amount"/>
            </element>
          </zeroOrMore>
        </element>
        <optional>
          <element name="railways">
            <element name="railways-total">
              <text/>
            </element>
            <optional>
              <element name="standard-gauge">
                <text/>
              </element>
            </optional>
            <optional>
              <element name="narrow-gauge">
                <text/>
              </element>
            </optional>
            <ref name="comparison"/>
          </element>
        </optional>
        <element name="roadways">
          <element name="roadways-total">
            <text/>
          </element>
          <optional>
            <element name="paved">
              <text/>
            </element>
          </optional>
          <optional>
            <element name="unpaved">
              <text/>
            </element>
          </optional>
          <ref name="comparison"/>
        </element>
        <element name="merchant-marine">
          <element name="merchant-total">
            <text/>
          </element>
          <element name="by-type">
            <text/>
          </element>
          <optional>
            <ref name="comparison"/>
          </optional>
        </element>
        <element name="ports-and-terminals">
          <optional>
            <element name="major-seaport">
              <text/>
            </element>
          </optional>
          <optional>
            <element name="oil-terminal">
              <text/>
            </element>
          </optional>
          <optional>
            <element name="bulk-cargo-port">
              <text/>
            </element>
          </optional>
          <optional>
            <element name="river-port">
              <text/>
            </element>
          </optional>
          <optional>
            <element name="lng-terminal">
              <text/>
            </element>
          </optional>
        </element>
      </element>
      <element name="military-and-security">
        <element name="military-branches">
          <text/>
        </element>
        <element name="military-service-age-and-obligation">
          <text/>
        </element>
      </element>
    </element>
  </start>
</grammar>
