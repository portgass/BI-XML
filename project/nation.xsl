<?xml version="1.0"?>
<xsl:stylesheet version="2.0" 
    xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
    <xsl:output method="html" indent="yes"/>

    <xsl:template name="main" match="/">
        <html>
            <head>
                <title>BI-XML project</title>
                <meta name="author" content="Petr Svoboda" />
                <link rel="stylesheet" href="style.css" />
            </head>
            <body>
                <h1>Nations</h1>

                <h2>Content</h2>

                <ul>
                    <xsl:for-each select="collection('.?select=nation*.xml')/nation">
                        <li>
                            <a href="nation-{@id}.html">
                                <xsl:value-of select="@name" />
                            </a>
                        </li>
                    </xsl:for-each>
                </ul>

                <xsl:apply-templates select="collection('.?select=nation*.xml')/nation" />
            </body>
        </html>
    </xsl:template>

    <xsl:template match="nation">
        <xsl:result-document method="html" href="nation-{@id}.html">
            <html>
                <head>
                    <title>BI-XML project</title>
                    <meta name="author" content="Petr Svoboda" />
                    <link rel="stylesheet" href="style.css" />
                </head>
                <body>
                    <h1 id="title">
                        <xsl:value-of select="@name" />
                    </h1>

                    <img src="images/{@name}.gif" alt="map" />

                    <a href="{source/@url}" class="source">
                        Source
                    </a>

                    <h2 id="contents">Contents</h2>

                    <ul>
                        <li><a href="#title">Title</a></li>
                        <li><a href="#contents">Contents</a></li>
                        <li><a href="#background">Background</a></li>
                        <li><a href="#geography">Geography</a></li>
                        <li><a href="#society">People and society</a></li>
                        <li><a href="#government">Government</a></li>
                        <li><a href="#economy">Economy</a></li>
                        <li><a href="#energy">Energy</a></li>
                        <li><a href="#communications">Communications</a></li>
                        <li><a href="#transportation">Transportation</a></li>
                        <li><a href="#military-and-security">Military and security</a></li>
                    </ul>

                    <h2 id="background">Background</h2>

                    <p><xsl:value-of select="introduction/background" /></p>

                    <xsl:apply-templates select="geography"/>
                    <xsl:apply-templates select="people-and-society"/>
                    <xsl:apply-templates select="government"/>
                    <xsl:apply-templates select="economy"/>
                    <xsl:apply-templates select="energy"/>
                    <xsl:apply-templates select="communications"/>
                    <xsl:apply-templates select="transportation"/>
                    <xsl:apply-templates select="military-and-security"/>
                </body>
            </html>
        </xsl:result-document>
    </xsl:template>

    <xsl:template name="block-1">
        <xsl:param name="element" />
        <xsl:param name="children" />
        <xsl:variable name="element-name">
            <xsl:value-of select="translate(local-name($element), '-', ' ')" />
        </xsl:variable>
        <xsl:variable name="element-name">
            <xsl:value-of select="concat(upper-case(
                substring($element-name,1,1)),
                substring($element-name, 2),
                ' '[not(last())])"
            />
        </xsl:variable>

        <div>
                
            <h3><xsl:value-of select="$element-name" /></h3>

            <xsl:if test="$element/country-comparison-to-the-world">
                <xsl:call-template name="world-compare">
                    <xsl:with-param name="rank" select="$element/country-comparison-to-the-world" />
                </xsl:call-template>
            </xsl:if>

            <xsl:choose>
                <xsl:when test="$children">
                    <xsl:for-each select="$children[not(self::country-comparison-to-the-world)]">
                        <xsl:call-template name="block-2">
                            <xsl:with-param name="element" select="." />
                        </xsl:call-template>
                    </xsl:for-each>
                </xsl:when>
                <xsl:otherwise>
                    <p><xsl:value-of select="$element" /></p>
                </xsl:otherwise>
            </xsl:choose>

        </div>
    </xsl:template>

    <xsl:template name="block-2">
        <xsl:param name="element" />
        <xsl:param name="children" />
        <xsl:variable name="element-name">
            <xsl:value-of select="translate(local-name($element), '-', ' ')" />
        </xsl:variable>
        <xsl:variable name="element-name">
            <xsl:value-of select="concat(upper-case(
                substring($element-name,1,1)),
                substring($element-name, 2),
                ' '[not(last())])"
            />
        </xsl:variable>

        <div>
                
            <h4><xsl:value-of select="$element-name" /></h4>
            <p><xsl:value-of select="$element" /></p>

        </div>
    </xsl:template>
    
    <xsl:template name="block-list">
        <xsl:param name="element" />
        <xsl:variable name="element-name">
            <xsl:value-of select="translate(local-name($element), '-', ' ')" />
        </xsl:variable>
        <xsl:variable name="element-name">
            <xsl:value-of select="concat(upper-case(
                substring($element-name,1,1)),
                substring($element-name, 2),
                ' '[not(last())])"
            />
        </xsl:variable>

        <div>
                
            <h3><xsl:value-of select="$element-name" /></h3>

            <ul>
                <xsl:for-each select="$element/*">
                    <li><xsl:value-of select="." /></li>
                </xsl:for-each>
            </ul>

        </div>
    </xsl:template>

    <xsl:template name="world-compare">
        <xsl:param name="rank" />
                
        <span class="rank">
            World rank: <xsl:value-of select="$rank" />
        </span>
    </xsl:template>

    <xsl:template match="geography">
        <section>
            <h2 id="geography">Geography</h2>

            <xsl:call-template name="block-1">
                <xsl:with-param name="element" select="location" />
            </xsl:call-template>
            <xsl:call-template name="block-1">
                <xsl:with-param name="element" select="geographic-coordinates" />
            </xsl:call-template>
            <xsl:call-template name="block-1">
                <xsl:with-param name="element" select="map-references" />
            </xsl:call-template>
            <xsl:call-template name="block-1">
                <xsl:with-param name="element" select="area" />
                <xsl:with-param name="children" select="area/*" />
            </xsl:call-template>
            <xsl:call-template name="block-1">
                <xsl:with-param name="element" select="area-comparative" />
            </xsl:call-template>
            <xsl:call-template name="block-1">
                <xsl:with-param name="element" select="land-boundaries" />
            </xsl:call-template>
            <xsl:call-template name="block-1">
                <xsl:with-param name="element" select="coastline" />
            </xsl:call-template>
            <xsl:call-template name="block-1">
                <xsl:with-param name="element" select="maritime-claims" />
                <xsl:with-param name="children" select="maritime-claims/*" />
            </xsl:call-template>
            <xsl:call-template name="block-1">
                <xsl:with-param name="element" select="climate" />
            </xsl:call-template>
            <xsl:call-template name="block-1">
                <xsl:with-param name="element" select="terrain" />
            </xsl:call-template>
            <xsl:call-template name="block-1">
                <xsl:with-param name="element" select="elevation" />
                <xsl:with-param name="children" select="elevation/*" />
            </xsl:call-template>
            <xsl:call-template name="block-list">
                <xsl:with-param name="element" select="natural-resources" />
            </xsl:call-template>
            <xsl:apply-templates select="land-use"/>
            <xsl:call-template name="block-1">
                <xsl:with-param name="element" select="irrigated-land" />
            </xsl:call-template>
            <xsl:call-template name="block-1">
                <xsl:with-param name="element" select="population-distribution" />
            </xsl:call-template>
            <xsl:call-template name="block-1">
                <xsl:with-param name="element" select="natural-hazards" />
            </xsl:call-template>
            <xsl:call-template name="block-1">
                <xsl:with-param name="element" select="environment-current-issues" />
            </xsl:call-template>
            <xsl:call-template name="block-1">
                <xsl:with-param name="element" select="geography-note" />
            </xsl:call-template>
        </section>
    </xsl:template>

    <xsl:template match="people-and-society">
        <section>
            <h2 id="society">Society</h2>

            <xsl:call-template name="block-1">
                <xsl:with-param name="element" select="population" />
            </xsl:call-template>
            <xsl:call-template name="block-1">
                <xsl:with-param name="element" select="nationality" />
                <xsl:with-param name="children" select="nationality/*" />
            </xsl:call-template>
            <xsl:apply-templates select="ethnic-groups"/>
            <xsl:apply-templates select="languages"/>
            <xsl:apply-templates select="religions"/>
            <xsl:apply-templates select="age"/>
            <div>
                <h4>Growth rate</h4>
                <xsl:value-of select="growth-rate/@percentage" />%
            </div>
            <div>
                <h4>Birth rate</h4>
                <xsl:value-of select="birth-rate/@amount" />&#160;<xsl:value-of select="birth-rate/@unit" />
            </div>
            <div>
                <h4>Death rate</h4>
                <xsl:value-of select="death-rate/@amount" />&#160;<xsl:value-of select="death-rate/@unit" />
            </div>
            <div>
                <h4>Migration rate</h4>
                <xsl:value-of select="migration-rate/@amount" />&#160;<xsl:value-of select="migration-rate/@unit" />
            </div>
            <xsl:call-template name="block-1">
                <xsl:with-param name="element" select="population-distribution" />
            </xsl:call-template>
            <xsl:call-template name="block-1">
                <xsl:with-param name="element" select="major-urban-areas-population" />
            </xsl:call-template>
            <xsl:apply-templates select="sex-ratios"/>
            <xsl:call-template name="block-1">
                <xsl:with-param name="element" select="infant-mortality-rate" />
                <xsl:with-param name="children" select="infant-mortality-rate/*" />
            </xsl:call-template>
            <xsl:call-template name="block-1">
                <xsl:with-param name="element" select="life-expectancy-at-birth" />
                <xsl:with-param name="children" select="life-expectancy-at-birth/*" />
            </xsl:call-template>
            <xsl:call-template name="block-1">
                <xsl:with-param name="element" select="total-fertility-rate" />
            </xsl:call-template>
        </section>
    </xsl:template>

    <xsl:template match="government">
        <section>
            <h2 id="government">Government</h2>

            <xsl:call-template name="block-1">
                <xsl:with-param name="element" select="country-name" />
                <xsl:with-param name="children" select="country-name/*" />
            </xsl:call-template>
            <xsl:call-template name="block-1">
                <xsl:with-param name="element" select="government-type" />
            </xsl:call-template>
            <xsl:call-template name="block-1">
                <xsl:with-param name="element" select="capital" />
                <xsl:with-param name="children" select="capital/*" />
            </xsl:call-template>
            <xsl:call-template name="block-1">
                <xsl:with-param name="element" select="administrative-divisions" />
            </xsl:call-template>
            <xsl:call-template name="block-1">
                <xsl:with-param name="element" select="national-holiday" />
            </xsl:call-template>
            <xsl:call-template name="block-1">
                <xsl:with-param name="element" select="constitution" />
            </xsl:call-template>
            <xsl:call-template name="block-1">
                <xsl:with-param name="element" select="legal-system" />
            </xsl:call-template>
            <xsl:call-template name="block-1">
                <xsl:with-param name="element" select="suffrage" />
            </xsl:call-template>
            <xsl:call-template name="block-1">
                <xsl:with-param name="element" select="executive-branch" />
                <xsl:with-param name="children" select="executive-branch/*" />
            </xsl:call-template>
            <xsl:call-template name="block-1">
                <xsl:with-param name="element" select="legislative-branch" />
                <xsl:with-param name="children" select="legislative-branch/*" />
            </xsl:call-template>
            <xsl:call-template name="block-1">
                <xsl:with-param name="element" select="judicial-branch" />
                <xsl:with-param name="children" select="judicial-branch/*" />
            </xsl:call-template>
            <xsl:call-template name="block-1">
                <xsl:with-param name="element" select="political-parties-and-leaders" />
            </xsl:call-template>
            <xsl:call-template name="block-1">
                <xsl:with-param name="element" select="flag-description" />
            </xsl:call-template>
            <xsl:call-template name="block-1">
                <xsl:with-param name="element" select="national-symbol" />
            </xsl:call-template>
            <xsl:call-template name="block-1">
                <xsl:with-param name="element" select="national-anthem" />
                <xsl:with-param name="children" select="national-anthem/*" />
            </xsl:call-template>
        </section>
    </xsl:template>

    <xsl:template match="economy">
        <section>
            <h2 id="economy">Economy</h2>

            <xsl:call-template name="block-1">
                <xsl:with-param name="element" select="economy-overview" />
            </xsl:call-template>
            <xsl:call-template name="block-1">
                <xsl:with-param name="element" select="gdp-purchasing-power-parity" />
            </xsl:call-template>
            <xsl:call-template name="block-1">
                <xsl:with-param name="element" select="gdp-official-exchange-rate" />
            </xsl:call-template>
            <xsl:call-template name="block-1">
                <xsl:with-param name="element" select="gdp-real-growth-rate" />
            </xsl:call-template>
            <xsl:call-template name="block-1">
                <xsl:with-param name="element" select="gdp-per-capita" />
            </xsl:call-template>
            <xsl:call-template name="block-1">
                <xsl:with-param name="element" select="by-end-use" />
                <xsl:with-param name="children" select="by-end-use/*" />
            </xsl:call-template>
            <xsl:call-template name="block-1">
                <xsl:with-param name="element" select="by-sector-of-origin" />
                <xsl:with-param name="children" select="by-sector-of-origin/*" />
            </xsl:call-template>
            <xsl:call-template name="block-list">
                <xsl:with-param name="element" select="agriculture-products" />
            </xsl:call-template>
            <xsl:call-template name="block-list">
                <xsl:with-param name="element" select="industries" />
            </xsl:call-template>
            <xsl:call-template name="block-1">
                <xsl:with-param name="element" select="labor-force" />
            </xsl:call-template>
            <xsl:call-template name="block-1">
                <xsl:with-param name="element" select="labor-force-by-occupation" />
                <xsl:with-param name="children" select="labor-force-by-occupation/*" />
            </xsl:call-template>
            <xsl:call-template name="block-1">
                <xsl:with-param name="element" select="unemployment-rate" />
            </xsl:call-template>
            <xsl:call-template name="block-1">
                <xsl:with-param name="element" select="taxes-and-other-revenues" />
            </xsl:call-template>
            <xsl:call-template name="block-1">
                <xsl:with-param name="element" select="public-debt" />
            </xsl:call-template>
            <xsl:call-template name="block-1">
                <xsl:with-param name="element" select="inflation-rate-consumer-prices" />
            </xsl:call-template>
            <xsl:call-template name="block-1">
                <xsl:with-param name="element" select="exports" />
            </xsl:call-template>
            <xsl:call-template name="block-1">
                <xsl:with-param name="element" select="exports-commodities" />
            </xsl:call-template>
            <xsl:call-template name="block-1">
                <xsl:with-param name="element" select="imports" />
            </xsl:call-template>
            <xsl:call-template name="block-1">
                <xsl:with-param name="element" select="imports-commodities" />
            </xsl:call-template>
            <xsl:call-template name="block-1">
                <xsl:with-param name="element" select="exchange-rates" />
            </xsl:call-template>
        </section>
    </xsl:template>

    <xsl:template match="energy">
        <section>
            <h2 id="energy">Energy</h2>

            <xsl:for-each select="./*">
                <xsl:call-template name="block-1">
                    <xsl:with-param name="element" select="." />
                </xsl:call-template>
            </xsl:for-each>
            
        </section>
    </xsl:template>

    <xsl:template match="communications">
        <section>
            <h2 id="communications">Communications</h2>

            <xsl:call-template name="block-1">
                <xsl:with-param name="element" select="telephone-system" />
            </xsl:call-template>
            <xsl:call-template name="block-1">
                <xsl:with-param name="element" select="broadcast-media" />
            </xsl:call-template>
            <xsl:call-template name="block-1">
                <xsl:with-param name="element" select="internet-country-code" />
            </xsl:call-template>
            <xsl:call-template name="block-1">
                <xsl:with-param name="element" select="internet-users" />
                <xsl:with-param name="children" select="internet-users/*" />
            </xsl:call-template>
        </section>
    </xsl:template>

    <xsl:template match="transportation">
        <section>
            <h2 id="transportation">Transportation</h2>

            <xsl:call-template name="block-1">
                <xsl:with-param name="element" select="national-air-transport-system" />
                <xsl:with-param name="children" select="national-air-transport-system/*" />
            </xsl:call-template>
            <xsl:call-template name="block-1">
                <xsl:with-param name="element" select="civil-aircraft-registration-country-code-prefix" />
            </xsl:call-template>
            <xsl:call-template name="block-1">
                <xsl:with-param name="element" select="airports" />
                <xsl:with-param name="children" select="airports/*" />
            </xsl:call-template>
            <xsl:apply-templates select="airports-with-paved-runways"/>
            <xsl:call-template name="block-1">
                <xsl:with-param name="element" select="railways" />
                <xsl:with-param name="children" select="railways/*" />
            </xsl:call-template>
            <xsl:call-template name="block-1">
                <xsl:with-param name="element" select="roadways" />
                <xsl:with-param name="children" select="roadways/*" />
            </xsl:call-template>
            <xsl:call-template name="block-1">
                <xsl:with-param name="element" select="merchant-marine" />
                <xsl:with-param name="children" select="merchant-marine/*" />
            </xsl:call-template>
            <xsl:call-template name="block-1">
                <xsl:with-param name="element" select="ports-and-terminals" />
                <xsl:with-param name="children" select="ports-and-terminals/*" />
            </xsl:call-template>
        </section>
    </xsl:template>

    <xsl:template match="military-and-security">
        <section>
            <h2 id="military-and-security">Military and security</h2>

            <xsl:call-template name="block-1">
                <xsl:with-param name="element" select="military-branches" />
            </xsl:call-template>
            <xsl:call-template name="block-1">
                <xsl:with-param name="element" select="military-service-age-and-obligation" />
            </xsl:call-template>
        </section>
    </xsl:template>

    <xsl:template match="ethnic-groups">
        <section>
            <h3 id="ethnic-groups">Ethnic groups</h3>

            <ul>
                <xsl:for-each select="ethnic-group">
                    <li>
                        <xsl:value-of select="@name" />
                        <xsl:if test="@percentage">
                            : <xsl:value-of select="@percentage" />%
                        </xsl:if>
                        <xsl:if test="@majority='true'">
                            (majority)
                        </xsl:if>
                    </li>
                </xsl:for-each>
            </ul>
        </section>
    </xsl:template>

    <xsl:template match="land-use">
        <div>
            <h3 id="ethnic-groups">Land use</h3>

            <ul>
                <xsl:for-each select="land-use-category">
                    <li>
                        <xsl:value-of select="@type" />: <xsl:value-of select="@percentage" />%
                    </li>
                </xsl:for-each>
            </ul>
        </div>
    </xsl:template>

    <xsl:template match="sex-ratios">
        <div>
            <h3 id="sex-ratios">Sex ratios</h3>

            <ul>
                <xsl:for-each select="sex-ratio">
                    <li>
                        <xsl:value-of select="@type" />: <xsl:value-of select="@amount" />
                    </li>
                </xsl:for-each>
            </ul>
        </div>
    </xsl:template>

    <xsl:template match="airports-with-paved-runways">
        <div>
            <h3 id="airports-with-paved-runways">Airports with paved runways</h3>

            <ul>
                <xsl:for-each select="airport">
                    <li>
                        <xsl:value-of select="@type" />: <xsl:value-of select="@amount" />
                    </li>
                </xsl:for-each>
            </ul>
        </div>
    </xsl:template>

    <xsl:template match="languages">
        <section>
            <h3 id="languages">Languages</h3>

            <ul>
                <xsl:for-each select="language">
                    <li>
                        <xsl:value-of select="@name" />
                        <xsl:if test="@percentage">
                            : <xsl:value-of select="@percentage" />%
                        </xsl:if>
                        <xsl:if test="@official='true'">
                            (official)
                        </xsl:if>
                    </li>
                </xsl:for-each>
            </ul>
        </section>
    </xsl:template>

    <xsl:template match="religions">
        <section>
            <h3 id="religions">Religions</h3>

            <ul>
                <xsl:for-each select="religion">
                    <li>
                        <xsl:value-of select="@name" />
                        <xsl:if test="@percentage">
                            : <xsl:value-of select="@percentage" />%
                        </xsl:if>
                        <xsl:if test="@official='true'">
                            (official)
                        </xsl:if>
                        <xsl:if test="branch">
                            <ul>
                                <xsl:for-each select="branch">
                                    <li><xsl:value-of select="@name" />: <xsl:value-of select="@percentage" />%</li>
                                </xsl:for-each>
                            </ul>
                        </xsl:if>
                    </li>
                </xsl:for-each>
            </ul>
        </section>
    </xsl:template>

    <xsl:template match="age">
        <section>
            <h3>Age</h3>

            <div>
                <h4>Median</h4>
                Total: <xsl:value-of select="median/@total" /><br />
                Male: <xsl:value-of select="median/@male" /><br />
                Female: <xsl:value-of select="median/@female" />
            </div>

            <div>
                <h4>Structure</h4>

                <ul>
                    <xsl:for-each select="age-structure/age-range">
                        <li>
                            <xsl:value-of select="@from" />
                            <xsl:choose>
                                <xsl:when test="@to">
                                    - <xsl:value-of select="@to" />
                                </xsl:when>
                                <xsl:otherwise>
                                    + 
                                </xsl:otherwise>
                            </xsl:choose>
                            : <xsl:value-of select="@percentage" />%

                            <ul>
                                <li>Male: <xsl:value-of select="male/@amount" /></li>
                                <li>Female: <xsl:value-of select="female/@amount" /></li>
                            </ul>
                        </li>
                    </xsl:for-each>
                </ul>
            </div>
        </section>
    </xsl:template>
</xsl:stylesheet>