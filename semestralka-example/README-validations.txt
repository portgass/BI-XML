XML SCHEMA: 	xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:noNamespaceSchemaLocation="xml-template.xsd"
           -> validation (http://tools.decisionsoft.com/schemaValidate/)
           -> xmllint --noout --schema xml-template.xsd xml-template.xml

RELAXNG:   -> xmllint --noout --relaxng xml-template.rng xml-template.xml (prevod z rnc na rng pres TRANG: 
                                                                           java -jar trang.jar xml-template.rnc xml-template.rng)

DOCTYPE:   <!DOCTYPE notebooks SYSTEM "./xml-template.dtd">
           -> validotvat pres SAXON (-dtd:on)


SCHEMATRON: 
           -> nejprve z xml-template.sch udelat .xsl pro validaci xmltemplate.xml
              (java net.sf.saxon.Transform -s:xml-template.sch -xsl:schematron-basic.xsl -o:validation.xsl)

           -> pak zvalidovat vstupni XML a vysledek ulozit do report.txt
              (java net.sf.saxon.Transform -s:xml-template.xml -xsl:validation.xsl -o:report.txt)



