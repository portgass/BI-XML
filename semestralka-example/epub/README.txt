CMD(saxon): java -jar saxon9he.jar ../xml-template.xml epub-transformation.xsl > epub-material.html

CMD(xalan): java org.apache.xalan.xslt.Process -in ../xml-template.xml -xsl epub-transformation.xsl -out epub-output.html

VALIDATION: http://threepress.org/document/epub-validate/