<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">

<xsl:output method="xml" doctype-public="-//W3C//DTD XHTML 1.1//EN" doctype-system="http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd"
            encoding="utf-8" indent="yes" />

<xsl:template match="/">
  <html xmlns="http://www.w3.org/1999/xhtml" xml:lang="cs">
    <head>
      <meta http-equiv="Content-Type" content="application/xhtml+xml; charset=utf-8" />
      <meta http-equiv="content-language" content="cs" />
      <title>Doprovodný materiál k prezentaci notebooků (ePub)</title>
      <link rel="stylesheet" href="epub-css.css" type="text/css" />
    </head>
  <body>
    <div id="content">
        <h1>Doprovodný materiál k prezentaci notebooků (ePub)</h1> 
         <!-- ====== content ====== --> 
        
                   <xsl:for-each select="notebooks/pc">
                     <div class="box">
                         <h2><xsl:value-of select="manufacturer" />&#160;<xsl:value-of select="name" /></h2>
                         
                         <xsl:if test="string(images)">
                            <div class="right-box">
                             <xsl:element name="img">
                        		   <xsl:attribute name="src">resources/<xsl:value-of select="@id" />/images/<xsl:value-of select="images/image" /></xsl:attribute>
                               <xsl:attribute name="class">main-logo</xsl:attribute>
                        	   </xsl:element>
                             <div class="img-box">                                
                                  <xsl:for-each select="images/image">                                                   
                                     <xsl:element name="img">
                                		   <xsl:attribute name="src">resources/<xsl:value-of select="../../@id" />/images/<xsl:value-of select="." /></xsl:attribute>
                                	   </xsl:element> 
                                  </xsl:for-each>
                                  <div class="clear"><xsl:text> </xsl:text></div>
                             </div>
                            </div>   
                         </xsl:if> 
                         
                         <h3>Procesor</h3>
                         <ul>
                            <li>Typ: <xsl:value-of select="procesor/p_type" /></li>
                            <li>Frekvence: <xsl:value-of select="procesor/p_frequence" /></li>
                         </ul>
                         
                         <h3>Operační pamět</h3>
                         <ul>
                            <li>Typ: <xsl:value-of select="memory/m_type" /></li>
                            <li>Velikost: <xsl:value-of select="memory/m_size" /></li>
                            <li>Frekvence: <xsl:value-of select="memory/m_frequence" /></li>
                         </ul>
                         
                         <h3>Operační systém</h3>
                         <ul>
                            <li><xsl:value-of select="os/o_type" /> (<xsl:value-of select="os/o_version" />)</li>
                         </ul>
                         
                         <h3>Grafika</h3>
                         <ul>
                            <li>Grafická karta: <xsl:value-of select="graphic/g_card/c_manufacturer" />&#160;<xsl:value-of select="graphic/g_card/c_name" /> (<xsl:value-of select="graphic/g_card/c_memory_size" />)</li>
                            <li>Display: <xsl:value-of select="graphic/g_display/d_size" />&#160;
                                <xsl:value-of select="graphic/g_display/d_technology" />, 
                                <xsl:value-of select="graphic/g_display/d_type" /> 
                                (<xsl:value-of select="graphic/g_display/d_resolution/r_width" /> x
                                 <xsl:value-of select="graphic/g_display/d_resolution/r_height" />)</li>
                         </ul>
                         
                         <h3>Pevný disk(y)</h3>
                         <ul>
                            <li>Velikost: <xsl:value-of select="storage/s_size" /></li>
                            <li>Počet: <xsl:value-of select="storage/s_count" /></li>
                         </ul>
                         
                         <h3>Mechanika(y)</h3>
                         <ul>
                            <xsl:for-each select="drives/drive"> 
                              <li><xsl:value-of select="." /></li>                                                  
                            </xsl:for-each>
                         </ul>
                         
                         <h3>Síťové připojení</h3>
                         <ul>
                            <xsl:for-each select="network/adapter"> 
                              <li><xsl:value-of select="." /></li>                                                  
                            </xsl:for-each>
                         </ul>
                         
                         <h3>Rozhraní</h3>
                         <ul>
                            <xsl:for-each select="accessories/accessory"> 
                              <li><xsl:value-of select="." /></li>                                                  
                            </xsl:for-each>
                         </ul>
                         
                         <h3>Výdrž a hmostnost</h3>
                         <ul>
                            <li>až <xsl:value-of select="battery" /> h.</li>
                            <li><xsl:value-of select="weight" /> kg</li>
                         </ul>
 
                         <h3>Popis</h3>
                         <xsl:for-each select="description/par">
                            <p><xsl:value-of select="." /></p>
                         </xsl:for-each>
                         <div class="clear"><xsl:text> </xsl:text></div>
                      </div>
                   </xsl:for-each>
                   
         <!-- ====== content end ====== -->
          
        </div>
    </body>
  </html>  
</xsl:template>

</xsl:stylesheet> 



  

  
  
