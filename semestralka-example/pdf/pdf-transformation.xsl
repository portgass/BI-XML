<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="1.0"
      xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
      xmlns:fo="http://www.w3.org/1999/XSL/Format">
  <xsl:output method="xml" indent="yes"/>
  <xsl:template match="/">
  
    <fo:root xmlns:fo="http://www.w3.org/1999/XSL/Format">
    
      <fo:layout-master-set>
         <fo:simple-page-master master-name="maintmpl" page-height="29.7cm" page-width="21.0cm"
                                margin-top="1.5cm" margin-bottom="1.5cm" margin-left="1.5cm" margin-right="1.5cm">
          
            <fo:region-body margin-top="1cm" margin-bottom="0.2cm" />
            <fo:region-before extent="1cm"/>
            <fo:region-after extent="0.5cm"/>
            
         </fo:simple-page-master>
     </fo:layout-master-set>
     
     <fo:page-sequence master-reference="maintmpl">
     
        <fo:static-content flow-name="xsl-region-before">
            <fo:block line-height="13pt" font-size="13pt" border-after-width="thin" border-after-style="solid" padding-bottom="2px"
                      text-align-last="justify">
                Doprovodný materiál k prezentaci notebooků (pdf)
                  <fo:leader leader-pattern="space" />
                kopcapet (BI-XML)
            </fo:block>
        </fo:static-content>
        
        <fo:static-content flow-name="xsl-region-after">
           <fo:block font-size="11pt" text-align="center" border-before-width="thin" border-before-style="solid" padding-top="10px" margin-top="5px">
            Strana <fo:inline font-weight="bold"><fo:page-number/></fo:inline> z <fo:page-number-citation ref-id="last-page"/>
           </fo:block>
        </fo:static-content>
        
        <fo:flow flow-name="xsl-region-body">
            <xsl:for-each select="notebooks/pc">
              <fo:block break-before="page">
              
                 <fo:block font-size="18pt" text-align="left" font-weight="bold">
                    <xsl:value-of select="manufacturer" /> <xsl:value-of select="name" />
                 </fo:block>
                 
                 
                  <fo:table>
                  <fo:table-column column-width="60%"/>
                  <fo:table-column column-width="40%"/>
                  
                  <fo:table-body>
                    <fo:table-row>
                      <fo:table-cell>
                        <fo:block>
                        
                        <fo:block font-size="14pt" font-weight="bold" text-align="left" space-before="10px" space-after="0px">Procesor</fo:block>
                 <fo:list-block provisional-distance-between-starts="10px"
                  start-indent="10px" font-size="12pt">

                      <fo:list-item>
                        <fo:list-item-label end-indent="label-end()">
                          <fo:block>&gt;</fo:block>
                        </fo:list-item-label>
                        <fo:list-item-body start-indent="body-start()">
                          <fo:block>Typ: <xsl:value-of select="procesor/p_type" /></fo:block>
                        </fo:list-item-body>
                      </fo:list-item>
                      
                      <fo:list-item>
                        <fo:list-item-label end-indent="label-end()">
                          <fo:block>&gt;</fo:block>
                        </fo:list-item-label>
                        <fo:list-item-body start-indent="body-start()">
                          <fo:block>Frekvence: <xsl:value-of select="procesor/p_frequence" /></fo:block>
                        </fo:list-item-body>
                      </fo:list-item>
                  
                  </fo:list-block>
                  
                  <fo:block font-size="14pt" font-weight="bold" text-align="left" space-before="10px" space-after="0px">Operační pamět</fo:block>
                  <fo:list-block provisional-distance-between-starts="10px"
                  start-indent="10px" font-size="12pt">

                      <fo:list-item>
                        <fo:list-item-label end-indent="label-end()">
                          <fo:block>&gt;</fo:block>
                        </fo:list-item-label>
                        <fo:list-item-body start-indent="body-start()">
                          <fo:block>Typ: <xsl:value-of select="memory/m_type" /></fo:block>
                        </fo:list-item-body>
                      </fo:list-item>
                      
                      <fo:list-item>
                        <fo:list-item-label end-indent="label-end()">
                          <fo:block>&gt;</fo:block>
                        </fo:list-item-label>
                        <fo:list-item-body start-indent="body-start()">
                          <fo:block>Velikost: <xsl:value-of select="memory/m_size" /></fo:block>
                        </fo:list-item-body>
                      </fo:list-item>
                      
                      <fo:list-item>
                        <fo:list-item-label end-indent="label-end()">
                          <fo:block>&gt;</fo:block>
                        </fo:list-item-label>
                        <fo:list-item-body start-indent="body-start()">
                          <fo:block>Frekvence: <xsl:value-of select="memory/m_frequence" /></fo:block>
                        </fo:list-item-body>
                      </fo:list-item>
                  
                   </fo:list-block>
                   
                  <fo:block font-size="14pt" font-weight="bold" text-align="left" space-before="10px" space-after="0px">Operační systém</fo:block>
                  <fo:list-block provisional-distance-between-starts="10px"
                  start-indent="10px" font-size="12pt">

                      <fo:list-item>
                        <fo:list-item-label end-indent="label-end()">
                          <fo:block>&gt;</fo:block>
                        </fo:list-item-label>
                        <fo:list-item-body start-indent="body-start()">
                          <fo:block><xsl:value-of select="os/o_type" /> (<xsl:value-of select="os/o_version" />)</fo:block>
                        </fo:list-item-body>
                      </fo:list-item>
                  
                   </fo:list-block>
                   
                  <fo:block font-size="14pt" font-weight="bold" text-align="left" space-before="10px" space-after="0px">Grafika</fo:block>
                  <fo:list-block provisional-distance-between-starts="10px"
                  start-indent="10px" font-size="12pt">

                      <fo:list-item>
                        <fo:list-item-label end-indent="label-end()">
                          <fo:block>&gt;</fo:block>
                        </fo:list-item-label>
                        <fo:list-item-body start-indent="body-start()">
                          <fo:block>Grafická karta: <xsl:value-of select="graphic/g_card/c_manufacturer" /> <xsl:value-of select="graphic/g_card/c_name" /> (<xsl:value-of select="graphic/g_card/c_memory_size" />)</fo:block>
                        </fo:list-item-body>
                      </fo:list-item>
                      
                      <fo:list-item>
                        <fo:list-item-label end-indent="label-end()">
                          <fo:block>&gt;</fo:block>
                        </fo:list-item-label>
                        <fo:list-item-body start-indent="body-start()">
                          <fo:block>Display: <xsl:value-of select="graphic/g_display/d_size" />&#160; 
                          <xsl:value-of select="graphic/g_display/d_technology" />, 
                          <xsl:value-of select="graphic/g_display/d_type" /> 
                          (<xsl:value-of select="graphic/g_display/d_resolution/r_width" /> x
                           <xsl:value-of select="graphic/g_display/d_resolution/r_height" />)</fo:block>
                        </fo:list-item-body>
                      </fo:list-item>
                  
                   </fo:list-block>
                   
                  <fo:block font-size="14pt" font-weight="bold" text-align="left" space-before="10px" space-after="0px">Pevný disk(y)</fo:block>
                  <fo:list-block provisional-distance-between-starts="10px"
                  start-indent="10px" font-size="12pt">

                      <fo:list-item>
                        <fo:list-item-label end-indent="label-end()">
                          <fo:block>&gt;</fo:block>
                        </fo:list-item-label>
                        <fo:list-item-body start-indent="body-start()">
                          <fo:block>Velikost: <xsl:value-of select="storage/s_size" /></fo:block>
                        </fo:list-item-body>
                      </fo:list-item>
                      
                      <fo:list-item>
                        <fo:list-item-label end-indent="label-end()">
                          <fo:block>&gt;</fo:block>
                        </fo:list-item-label>
                        <fo:list-item-body start-indent="body-start()">
                          <fo:block>Počet: <xsl:value-of select="storage/s_count" /></fo:block>
                        </fo:list-item-body>
                      </fo:list-item>
                  
                   </fo:list-block>
                   
                   <fo:block font-size="14pt" font-weight="bold" text-align="left" space-before="10px" space-after="0px">Mechanika(y)</fo:block>
                  <fo:list-block provisional-distance-between-starts="10px"
                  start-indent="10px" font-size="12pt">
                      
                      <xsl:for-each select="drives/drive"> 
                      
                         <fo:list-item>
                        <fo:list-item-label end-indent="label-end()">
                          <fo:block>&gt;</fo:block>
                        </fo:list-item-label>
                        <fo:list-item-body start-indent="body-start()">
                          <fo:block><xsl:value-of select="." /></fo:block>
                        </fo:list-item-body>
                      </fo:list-item>
                                                                  
                      </xsl:for-each>
                      
                  
                   </fo:list-block>
                   
                   <fo:block font-size="14pt" font-weight="bold" text-align="left" space-before="10px" space-after="0px">Síťové připojení</fo:block>
                  <fo:list-block provisional-distance-between-starts="10px"
                  start-indent="10px" font-size="12pt">
                      
                      <xsl:for-each select="network/adapter"> 
                      
                         <fo:list-item>
                        <fo:list-item-label end-indent="label-end()">
                          <fo:block>&gt;</fo:block>
                        </fo:list-item-label>
                        <fo:list-item-body start-indent="body-start()">
                          <fo:block><xsl:value-of select="." /></fo:block>
                        </fo:list-item-body>
                      </fo:list-item>
                                                                  
                      </xsl:for-each>
                      
                  
                   </fo:list-block>
                   
                   <fo:block font-size="14pt" font-weight="bold" text-align="left" space-before="10px" space-after="0px">Rozhraní</fo:block>
                  <fo:list-block provisional-distance-between-starts="10px"
                  start-indent="10px" font-size="12pt">
                      
                      <xsl:for-each select="accessories/accessory"> 
                      
                         <fo:list-item>
                        <fo:list-item-label end-indent="label-end()">
                          <fo:block>&gt;</fo:block>
                        </fo:list-item-label>
                        <fo:list-item-body start-indent="body-start()">
                          <fo:block><xsl:value-of select="." /></fo:block>
                        </fo:list-item-body>
                      </fo:list-item>
                                                                  
                      </xsl:for-each>
                      
                  
                   </fo:list-block>
                   
                   <fo:block font-size="14pt" font-weight="bold" text-align="left" space-before="10px" space-after="0px">Výdrž a hmostnost</fo:block>
                  <fo:list-block provisional-distance-between-starts="10px"
                  start-indent="10px" font-size="12pt">

                      <fo:list-item>
                        <fo:list-item-label end-indent="label-end()">
                          <fo:block>&gt;</fo:block>
                        </fo:list-item-label>
                        <fo:list-item-body start-indent="body-start()">
                          <fo:block>až <xsl:value-of select="battery" /> h.</fo:block>
                        </fo:list-item-body>
                      </fo:list-item>
                      
                      <fo:list-item>
                        <fo:list-item-label end-indent="label-end()">
                          <fo:block>&gt;</fo:block>
                        </fo:list-item-label>
                        <fo:list-item-body start-indent="body-start()">
                          <fo:block><xsl:value-of select="weight" /> kg</fo:block>
                        </fo:list-item-body>
                      </fo:list-item>
                  
                   </fo:list-block>
                        
                        </fo:block>
                      </fo:table-cell>
                      <fo:table-cell>
                        <fo:block text-align="right">
                        
                        <fo:block border-after-width="thin" border-after-style="dashed" padding-bottom="2px" margin-bottom="2px">
                        <xsl:if test="string(images)">
                           <xsl:element name="fo:external-graphic">
                             <xsl:attribute name="src">
                                ../resources/<xsl:value-of select="@id" />/images/<xsl:value-of select="images/image" />
                             </xsl:attribute>
                             <xsl:attribute name="content-width">200px</xsl:attribute> 
                             <xsl:attribute name="width">200px</xsl:attribute>
                           </xsl:element>       
                       </xsl:if>
                       </fo:block>
                       
                       <fo:table>
                        <fo:table-column column-width="33%"/>
                        <fo:table-column column-width="33%"/>
                        <fo:table-column column-width="33%"/>
                        
                        <fo:table-body>
                          <fo:table-row>

                                  <xsl:for-each select="images/image">
                                     <fo:table-cell>
                                     <fo:block text-align="center">                                                   
                                       <xsl:element name="fo:external-graphic">
                                  		   <xsl:attribute name="src">
                                  			       ../resources/<xsl:value-of select="../../@id" />/images/<xsl:value-of select="." />
                                         </xsl:attribute>
                                         <xsl:attribute name="content-width">60px</xsl:attribute> 
                                         <xsl:attribute name="content-height">40px</xsl:attribute> 
                                         <xsl:attribute name="width">60px</xsl:attribute>
                                         <xsl:attribute name="height">40px</xsl:attribute> 
                                  	   </xsl:element> 
                                	   </fo:block>
                                	   </fo:table-cell>
                                  </xsl:for-each>  
       
                          </fo:table-row>
                        </fo:table-body>
                       </fo:table>
                              
                        </fo:block>
                      </fo:table-cell>
                    </fo:table-row>
                  </fo:table-body>
                  
                  </fo:table>
                       

                                    
                 
                   <fo:block font-size="14pt" font-weight="bold" text-align="left" space-before="10px" space-after="0px">Popis</fo:block>
                   
                   <xsl:for-each select="description/par">
                     <fo:block font-size="12pt" text-align="justify" space-before="10px" space-after="0px">
                        <xsl:value-of select="." />
                     </fo:block>
                   </xsl:for-each>
                   
            
              </fo:block>
            </xsl:for-each>
            <fo:block id="last-page"/>
        </fo:flow>
        
     </fo:page-sequence>
     
    </fo:root>
    
  </xsl:template>
</xsl:stylesheet>
