<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">

<xsl:output method="xml" doctype-public="-//W3C//DTD SMIL 3.0 Language//EN" doctype-system="http://www.w3.org/2008/SMIL30/SMIL30Language.dtd"
            encoding="utf-8" indent="yes" />

<xsl:template match="/">
<smil xmlns="http://www.w3.org/ns/SMIL" version="3.0" baseProfile="Language">
  <head>
     <textStyling>
     <textStyle xml:id="header-style" textFontFamily="serif"
             textFontWeight="bold" textFontStyle="normal"          
             textWrapOption="noWrap" textColor="white" />
     <textStyle xml:id="slide-name" textFontFamily="serif"
             textFontWeight="bold" textFontStyle="reverseOblique"          
             textWrapOption="wrap" textFontSize="18px" />
    <textStyle xml:id="list-header" textFontFamily="serif"
             textFontWeight="bold" textFontStyle="normal"          
             textWrapOption="wrap" textFontSize="16px" />
    <textStyle xml:id="list-header-item" textFontFamily="serif"
             textFontWeight="normal" textFontStyle="normal"          
             textWrapOption="wrap" textFontSize="13px" />
     </textStyling>
     <layout>
        <root-layout width="980" height="550" />
          <region xml:id="bg-fill" top="0" left="0" width="980" height="550" />
          <region xml:id="music"/>
          
          <region xml:id="header" top="5" left="30" width="920" height="37" textStyle="header-style" />
          <region xml:id="line" top="43" left="25" width="930" height="2" backgroundColor="#edaa09" />
          
          <region xml:id="content" top="65" left="35" width="490" height="470" backgroundColor="white" fit="hidden" z-index="10" />
          <region xml:id="content-fill" top="55" left="25" width="510" height="490" z-index="0" />
          
          <region xml:id="multimedia-content" top="65" right="35" width="390" height="470" z-index="10">
            <region xml:id="img-content" top="0" width="100%" height="230" fit="meetBest" z-index="15" />
            <region xml:id="video-content" bottom="0" width="100%" height="230" fit="meetBest" z-index="15" />
          </region>
          
          <region xml:id="multimedia-content-fill" top="55" right="25" width="410" height="490" z-index="0" />
          
          <region xml:id="last-slide" top="65" left="35" width="910" height="470" z-index="25" />
          <region xml:id="last-slide-fill" top="55" left="25" width="930" height="490" z-index="20" />
     </layout>
     <transition xml:id="fade" type="fade" />
  </head>
  <body>
   
    <par>  
        <xsl:element name="img">
    		   <xsl:attribute name="src">bg-fill.png</xsl:attribute>
           <xsl:attribute name="xml:id">img_bg_fill</xsl:attribute>
           <xsl:attribute name="region">bg-fill</xsl:attribute> 
           <xsl:attribute name="fill">freeze</xsl:attribute>                    
        </xsl:element>
        
        <xsl:element name="img">
    		   <xsl:attribute name="src">content-fill.png</xsl:attribute>
           <xsl:attribute name="xml:id">img_content_fill</xsl:attribute>
           <xsl:attribute name="region">content-fill</xsl:attribute> 
           <xsl:attribute name="fill">freeze</xsl:attribute>                    
        </xsl:element>
        
        <xsl:element name="img">
    		   <xsl:attribute name="src">multimedia-content-fill.png</xsl:attribute>
           <xsl:attribute name="xml:id">img_multimedia_content_fill</xsl:attribute>
           <xsl:attribute name="region">multimedia-content-fill</xsl:attribute> 
           <xsl:attribute name="fill">freeze</xsl:attribute>                    
        </xsl:element>
      
        <par>
        <smilText textAlign="left" top="14" textFontSize="18px" xml:id="header-title" region="header" fill="freeze" dur="3s" transIn="fade">
             Prezentace notebooků            
        </smilText>
        <smilText textAlign="right" top="19" textFontSize="13px" xml:id="header-name" region="header" fill="freeze" dur="3s" transIn="fade">
             kopcapet (BI-XML)        
        </smilText>
        </par>
      
        <audio begin="2s" xml:id="bg-music" region="music" src="bg_song.mp3" soundLevel="30%"/>
      <seq>
        <xsl:for-each select="notebooks/pc">
            <xsl:variable name="first-ntb">
                <xsl:choose>
                 <xsl:when test="position() = 1">1</xsl:when>
                 <xsl:otherwise>0</xsl:otherwise>
                </xsl:choose>
            </xsl:variable>
            
            <par> 
              <xsl:element name="smilText">
                 <xsl:attribute name="xml:id">ntb_name<xsl:value-of select="@id" /></xsl:attribute>
                 <xsl:attribute name="region">content</xsl:attribute>
                 <xsl:attribute name="dur">45s</xsl:attribute>
                 <xsl:attribute name="fill">freeze</xsl:attribute>
                 <xsl:if test="$first-ntb = 1">
                    <xsl:attribute name="begin">2s</xsl:attribute>
                 </xsl:if>  
                 
                 <div textStyle="slide-name"><xsl:value-of select="manufacturer" />&#160;<xsl:value-of select="name" /></div>
              </xsl:element>
              
              <xsl:element name="smilText">
                 <xsl:attribute name="xml:id">ntb_info<xsl:value-of select="@id" /></xsl:attribute>
                 <xsl:attribute name="top">30</xsl:attribute>
                 <xsl:attribute name="region">content</xsl:attribute>
                 <xsl:attribute name="dur">45s</xsl:attribute>
                 <xsl:attribute name="fill">freeze</xsl:attribute>
                 <xsl:if test="$first-ntb = 1">
                    <xsl:attribute name="begin">2s</xsl:attribute>
                 </xsl:if>  
                 
                 
                 <!-- ===== first item ===== -->
                 <div textStyle="list-header">Procesor<br />               
                   <span textStyle="list-header-item">&#160;&#160;&#160;&gt;&#160;Typ: <xsl:value-of select="procesor/p_type" /></span><br />
                   <span textStyle="list-header-item">&#160;&#160;&#160;&gt;&#160;Frekvence: <xsl:value-of select="procesor/p_frequence" /></span>
                 </div> 
                 
                 <!-- next tev tag -->
                 <xsl:element name="tev">
                    <xsl:attribute name="xml:id">ntb_info_text<xsl:value-of select="@id" />_1</xsl:attribute>
                    <xsl:attribute name="next">4s</xsl:attribute>
                 </xsl:element>
                 
                 
                 <!-- ===== second item ===== -->
                 <div textStyle="list-header">Operační pamět<br />        
                   <span textStyle="list-header-item">&#160;&#160;&#160;&gt;&#160;Typ: <xsl:value-of select="memory/m_type" /></span><br />  
                   <span textStyle="list-header-item">&#160;&#160;&#160;&gt;&#160;Velikost: <xsl:value-of select="memory/m_size" /></span><br />  
                   <span textStyle="list-header-item">&#160;&#160;&#160;&gt;&#160;Frekvence: <xsl:value-of select="memory/m_frequence" /></span>
                 </div>
                                
                 <!-- next tev tag -->
                 <xsl:element name="tev">
                    <xsl:attribute name="xml:id">ntb_info_text<xsl:value-of select="@id" />_2</xsl:attribute>
                    <xsl:attribute name="next">4s</xsl:attribute>
                 </xsl:element>
                 
                 
                 <!-- ===== third item ===== -->
                 <div textStyle="list-header">Operační systém<br />           
                   <span textStyle="list-header-item">&#160;&#160;&#160;&gt;&#160;<xsl:value-of select="os/o_type" /> (<xsl:value-of select="os/o_version" />)</span>
                 </div>
                                
                 <!-- next tev tag -->
                 <xsl:element name="tev">
                    <xsl:attribute name="xml:id">ntb_info_text<xsl:value-of select="@id" />_3</xsl:attribute>
                    <xsl:attribute name="next">4s</xsl:attribute>
                 </xsl:element>
                 
                 
                 <!-- ===== fourth item ===== -->
                 <div textStyle="list-header">Grafika<br />             
                   <span textStyle="list-header-item">&#160;&#160;&#160;&gt;&#160;Grafická karta: <xsl:value-of select="graphic/g_card/c_manufacturer" />&#160;<xsl:value-of select="graphic/g_card/c_name" /> (<xsl:value-of select="graphic/g_card/c_memory_size" />)</span><br />
                   <span textStyle="list-header-item">&#160;&#160;&#160;&gt;&#160;Display: <xsl:value-of select="graphic/g_display/d_size" />&#160;
                                <xsl:value-of select="graphic/g_display/d_technology" />, 
                                <xsl:value-of select="graphic/g_display/d_type" /> 
                                (<xsl:value-of select="graphic/g_display/d_resolution/r_width" /> x
                                 <xsl:value-of select="graphic/g_display/d_resolution/r_height" />)</span>
                 </div>
                                
                 <!-- next tev tag -->
                 <xsl:element name="tev">
                    <xsl:attribute name="xml:id">ntb_info_text<xsl:value-of select="@id" />_4</xsl:attribute>
                    <xsl:attribute name="next">4s</xsl:attribute>
                 </xsl:element>
                 
                 
                 <!-- ===== fifth item ===== -->
                 <div textStyle="list-header">Pevný disk(y)<br />           
                   <span textStyle="list-header-item">&#160;&#160;&#160;&gt;&#160;Velikost: <xsl:value-of select="storage/s_size" /></span><br />
                   <span textStyle="list-header-item">&#160;&#160;&#160;&gt;&#160;Počet: <xsl:value-of select="storage/s_count" /></span>
                 </div>
                                
                 <!-- next tev tag -->
                 <xsl:element name="tev">
                    <xsl:attribute name="xml:id">ntb_info_text<xsl:value-of select="@id" />_5</xsl:attribute>
                    <xsl:attribute name="next">4s</xsl:attribute>
                 </xsl:element>
                 
                 
                 <!-- ===== sixth item ===== -->
                 <div textStyle="list-header">Mechanika(y)<br />       
                 <xsl:for-each select="drives/drive"> 
                      <span textStyle="list-header-item">&#160;&#160;&#160;&gt;&#160;<xsl:value-of select="." /></span>
                      <xsl:if test="position() != last()">
                         <br /> 
                      </xsl:if>                                              
                 </xsl:for-each>
                 </div>
                 
                 <!-- next tev tag -->
                 <xsl:element name="tev">
                    <xsl:attribute name="xml:id">ntb_info_text<xsl:value-of select="@id" />_6</xsl:attribute>
                    <xsl:attribute name="next">4s</xsl:attribute>
                 </xsl:element>
                 
                 
                 <!-- ===== seventh item ===== -->
                 <div textStyle="list-header">Výdrž a hmostnost<br />           
                   <span textStyle="list-header-item">&#160;&#160;&#160;&gt;&#160;až <xsl:value-of select="battery" /> h.</span><br />
                   <span textStyle="list-header-item">&#160;&#160;&#160;&gt;&#160;<xsl:value-of select="weight" /> kg</span>
                 </div>
              
                 <!-- last CLEAR tag -->
                 <xsl:element name="clear">
                    <xsl:attribute name="xml:id">ntb_info_text<xsl:value-of select="@id" />_5</xsl:attribute>
                    <xsl:attribute name="next">4s</xsl:attribute>
                 </xsl:element>
                 
                 <!-- ===== last item (desc) ===== -->
                 <div textStyle="list-header">Popis<br />
                    <xsl:for-each select="description/par">
                      <p textStyle="list-header-item"><xsl:value-of select="." /></p>
                      
                      <!-- next tev tag -->
                      <xsl:element name="tev">
                          <xsl:attribute name="xml:id">ntb_info_desctext<xsl:value-of select="@id" />_<xsl:value-of select="position()"/></xsl:attribute>
                          <xsl:attribute name="next">1s</xsl:attribute>
                      </xsl:element>
                    </xsl:for-each>       
                 </div>
                 
              </xsl:element>
              
              <xsl:if test="string(images)">
                 <seq>
                     <xsl:for-each select="images/image"> 
                         <xsl:element name="img">
                    		   <xsl:attribute name="src">../resources/<xsl:value-of select="../../@id" />/images/<xsl:value-of select="." /></xsl:attribute>
                           <xsl:attribute name="dur">15s</xsl:attribute>
                           <xsl:attribute name="xml:id">img_<xsl:value-of select="../../@id" />_<xsl:value-of select="position()"/></xsl:attribute>
                           <xsl:attribute name="region">img-content</xsl:attribute>
                           <xsl:attribute name="transIn">fade</xsl:attribute>
                           <xsl:attribute name="regPoint">center</xsl:attribute>
                           <xsl:attribute name="regAlign">center</xsl:attribute>
                           
                           <xsl:if test="position() = last()">
                              <xsl:attribute name="fill">freeze</xsl:attribute>
                           </xsl:if> 
                           
                           <xsl:if test="$first-ntb = 1 and position() = 1">
                                 <xsl:attribute name="begin">2s</xsl:attribute>
                           </xsl:if>
                    	   </xsl:element>
                	   </xsl:for-each>
                 </seq>  
              </xsl:if> 
              
              <xsl:if test="string(video)">
                 <xsl:element name="video">
              		   <xsl:attribute name="src">../resources/<xsl:value-of select="@id" />/video/<xsl:value-of select="video" /></xsl:attribute>
                     <xsl:attribute name="xml:id">video<xsl:value-of select="@id" /></xsl:attribute>
                     <xsl:attribute name="region">video-content</xsl:attribute>
                     <xsl:attribute name="transIn">fade</xsl:attribute>
                     <xsl:attribute name="soundLevel">0%</xsl:attribute>
                     <xsl:attribute name="regPoint">center</xsl:attribute>
                     <xsl:attribute name="regAlign">center</xsl:attribute>
                     <xsl:if test="$first-ntb = 1">
                        <xsl:attribute name="begin">2s</xsl:attribute>
                     </xsl:if>
                 </xsl:element>
              </xsl:if> 
              
            </par>
        
        </xsl:for-each>
        
        <par>
           <xsl:element name="img">
      		   <xsl:attribute name="src">last-slide-fill.png</xsl:attribute>
             <xsl:attribute name="xml:id">img_last_slide_fill</xsl:attribute>
             <xsl:attribute name="region">last-slide-fill</xsl:attribute> 
             <xsl:attribute name="fill">freeze</xsl:attribute>                    
           </xsl:element>
        
           <smilText textAlign="center" top="14" textFontSize="18px" xml:id="last-slide-title" region="last-slide" fill="freeze" dur="3s" transIn="fade" regPoint="center" regAlign="center">
             Děkuji za pozornost !    
           </smilText>
           
        </par>
        
      </seq>
    
    </par>

  </body>
</smil>
</xsl:template>
</xsl:stylesheet> 