<?xml version="1.0" encoding="utf-8"?>
<schema xmlns="http://www.ascc.net/xml/schematron">

  <!-- notebook elementy -->
  <pattern name="Seznam notebooků je prázdný">
    <rule context="/notebooks">
      <report test="count(pc) &lt; 1">element notebooks musí obsahovat alespoň jednu položku pc</report>
    </rule>
  </pattern>
  
  
  <!-- pc elementy a atributy -->
  <pattern name="Element pc musí obsahovat všechny následující elementy">
    <rule context="/notebooks/pc">   
      <report test="count(name) != 1">element pc musí obsahovat právě jednu položku name</report>
      <report test="count(manufacturer) != 1">element pc musí obsahovat právě jednu položku manufacturer</report>
      <report test="count(procesor) != 1">element pc musí obsahovat právě jednu položku procesor</report>
      <report test="count(memory) != 1">element pc musí obsahovat právě jednu položku memory</report>
      <report test="count(os) != 1">element pc musí obsahovat právě jednu položku os</report>
      <report test="count(graphic) != 1">element pc musí obsahovat právě jednu položku graphic</report>
      <report test="count(storage) != 1">element pc musí obsahovat právě jednu položku storage</report>
      <report test="count(drives) != 1">element pc musí obsahovat právě jednu položku drives</report>
      <report test="count(battery) != 1">element pc musí obsahovat právě jednu položku battery</report>
      <report test="count(weight) != 1">element pc musí obsahovat právě jednu položku weight</report>
      <report test="count(network) != 1">element pc musí obsahovat právě jednu položku network</report>
      <report test="count(accessories) != 1">element pc musí obsahovat právě jednu položku accesorries</report>
      <report test="count(images) != 1">element pc musí obsahovat právě jednu položku images</report>
      <report test="count(video) != 1">element pc musí obsahovat právě jednu položku video</report>
      <report test="count(description) != 1">element pc musí obsahovat právě jednu položku description</report>  
      <report test="count(@id) != 1">element pc musí obsahovat právě jeden unikátní atribut id</report>    
    </rule>
  </pattern>
  
  
  <!-- procesor elementy -->
  <pattern name="Element procesor musí obsahovat všechny následující elementy">
    <rule context="/notebooks/pc/procesor">   
      <report test="count(p_type) != 1">element procesor musí obsahovat právě jednu položku p_type</report>
      <report test="count(p_frequence) != 1">element procesor musí obsahovat právě jednu položku p_frequence</report>  
    </rule>
  </pattern>
  
  <!-- memory elementy -->
  <pattern name="Element memory musí obsahovat všechny následující elementy">
    <rule context="/notebooks/pc/memory">   
      <report test="count(m_type) != 1">element memory musí obsahovat právě jednu položku m_type</report>
      <report test="count(m_size) != 1">element memory musí obsahovat právě jednu položku m_size</report>
      <report test="count(m_frequence) != 1">element memory musí obsahovat právě jednu položku m_frequence</report>  
    </rule>
  </pattern>
  
  <!-- operating system elementy -->
  <pattern name="Element os musí obsahovat všechny následující elementy">
    <rule context="/notebooks/pc/os">   
      <report test="count(o_type) != 1">element os musí obsahovat právě jednu položku o_type</report>
      <report test="count(o_version) != 1">element os musí obsahovat právě jednu položku o_version</report>
    </rule>
  </pattern>
  
  <!-- graphic elementy -->
  <pattern name="Element graphic musí obsahovat všechny následující elementy">
    <rule context="/notebooks/pc/graphic">   
      <report test="count(g_card) != 1">element graphic musí obsahovat právě jednu položku g_card</report>
      <report test="count(g_display) != 1">element graphic musí obsahovat právě jednu položku g_display</report>
    </rule>
  </pattern>
  
  <!-- storage elementy -->
  <pattern name="Element storage musí obsahovat všechny následující elementy">
    <rule context="/notebooks/pc/storage">   
      <report test="count(s_size) != 1">element storage musí obsahovat právě jednu položku s_size</report>
      <report test="count(s_count) != 1">element storage musí obsahovat právě jednu položku s_count</report>
    </rule>
  </pattern>
  
  <!-- drives elementy -->
  <pattern name="Element drives musí obsahovat všechny následující elementy">
    <rule context="/notebooks/pc/drives">   
      <report test="count(drive) &lt; 1">element drives musí obsahovat alespoň jednu položku drive</report>
    </rule>
  </pattern>
  
  <!-- network elementy -->
  <pattern name="Element network musí obsahovat všechny následující elementy">
    <rule context="/notebooks/pc/network">   
      <report test="count(adapter) &lt; 0">element network může obsahovat žádnou anebo libovolný počet položek adapter</report>
    </rule>
  </pattern>
  
  <!-- accessories elementy -->
  <pattern name="Element accessories musí obsahovat všechny následující elementy">
    <rule context="/notebooks/pc/accessories">   
      <report test="count(accessory) &lt; 0">element accessories může obsahovat žádnou anebo libovolný počet položek accessory</report>
    </rule>
  </pattern>
  
  <!-- images elementy -->
  <pattern name="Element images musí obsahovat všechny následující elementy">
    <rule context="/notebooks/pc/images">   
      <report test="count(image) &lt; 1">element images musí obsahovat alespoň jednu položku image</report>
    </rule>
  </pattern>
  
  <!-- description elementy -->
  <pattern name="Element description musí obsahovat všechny následující elementy">
    <rule context="/notebooks/pc/description">   
      <report test="count(par) &lt; 1">element description musí obsahovat alespoň jednu položku par</report>
    </rule>
  </pattern>
  
  
  <!-- g_card elementy -->
  <pattern name="Element g_card musí obsahovat všechny následující elementy">
    <rule context="/notebooks/pc/graphic/g_card">   
      <report test="count(c_name) != 1">element g_card musí obsahovat právě jednu položku c_name</report>
      <report test="count(c_manufacturer) != 1">element g_card musí obsahovat právě jednu položku c_manufacturer</report>
      <report test="count(c_memory_size) != 1">element g_card musí obsahovat právě jednu položku c_memory_size</report>  
    </rule>
  </pattern>
  
  <!-- g_display elementy -->
  <pattern name="Element g_display musí obsahovat všechny následující elementy">
    <rule context="/notebooks/pc/graphic/g_display">   
      <report test="count(d_technology) != 1">element g_display musí obsahovat právě jednu položku d_technology</report>
      <report test="count(d_type) != 1">element g_display musí obsahovat právě jednu položku d_type</report>
      <report test="count(d_size) != 1">element g_display musí obsahovat právě jednu položku d_size</report>
      <report test="count(d_resolution) != 1">element g_display musí obsahovat právě jednu položku d_resolution</report>  
    </rule>
  </pattern>
    
    
  <!-- d_resolution elementy -->
  <pattern name="Element d_resolution musí obsahovat všechny následující elementy">
    <rule context="/notebooks/pc/graphic/g_display/d_resolution">   
      <report test="count(r_width) != 1">element d_resolution musí obsahovat právě jednu položku r_width</report>
      <report test="count(r_height) != 1">element d_resolution musí obsahovat právě jednu položku r_height</report> 
    </rule>
  </pattern>
  
  <!-- unikatnost pc atributu id -->
  <pattern name="Duplicita čísel počítačů">
    <rule context="/notebooks/pc">
      <report test="count(../pc[@id = current()/@id]) &gt; 1">Duplicitní id: <value-of select="@id"/> u elementu <name/>.</report>
    </rule>
  </pattern>
  
</schema>
