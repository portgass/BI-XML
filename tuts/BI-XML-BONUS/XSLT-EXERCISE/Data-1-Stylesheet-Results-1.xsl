<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet xmlns:xsl='http://www.w3.org/1999/XSL/Transform' version="1.0">

<xsl:output method="html"/>

<xsl:template match="/article">
    <html>
        <head>
            <title><xsl:value-of select="./articleinfo/title"/></title>
            <style type="text/css">
                body{font-family:Sans-serif;}
            </style>
        </head>
        <body>
            <h1><xsl:value-of select="./articleinfo/title"/></h1>
            <p>
                <b><xsl:value-of select="./articleinfo/publisher/publishername"/></b>
                <xsl:text> </xsl:text>
                <xsl:value-of select="./articleinfo/pubdate"/>
                <xsl:text>, </xsl:text>
                <b><xsl:value-of select="./articleinfo/volumenum"/></b><xsl:text>:</xsl:text>
                <xsl:value-of select="./articleinfo/issuenum"/>
            </p>
            <xsl:apply-templates select="./sect1"/>
        </body>
    </html>
</xsl:template>

<xsl:template match="title[parent::sect1]">
    <h2><xsl:value-of select="."/></h2>
</xsl:template>

<xsl:template match="title[parent::sect2]">
    <h3><xsl:value-of select="."/></h3>
</xsl:template>

<xsl:template match="itemizedlist">
    <ul><xsl:apply-templates select="./listitem"/></ul>
</xsl:template>

<xsl:template match="listitem">
    <li><xsl:apply-templates/></li>
</xsl:template>

<xsl:template match="para">
    <p><xsl:apply-templates/></p>
</xsl:template>

<xsl:template match="ulink">
    <a href="{@url}"><xsl:value-of select="."/></a>
</xsl:template>

<xsl:template match="email|quote">
    <i><xsl:value-of select="."/></i>
</xsl:template>

</xsl:stylesheet>
