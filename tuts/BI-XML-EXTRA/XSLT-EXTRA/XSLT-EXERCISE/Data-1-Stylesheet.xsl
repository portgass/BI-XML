<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet xmlns:xsl='http://www.w3.org/1999/XSL/Transform' version="3.0">

<xsl:output method="html"/>

<xsl:template match="/">
    <html>
        <head>
            <title><xsl:value-of select="/article/articleinfo/title"/></title>
        </head>
        <body>
            <xsl:apply-templates/>
        </body>
    </html>
</xsl:template>

<xsl:template match="articleinfo">
    <div>
        <xsl:apply-templates select="volumenum"/>
        <xsl:apply-templates select="issuenum"/>
        <xsl:apply-templates select="publisher"/>
        <xsl:apply-templates select="pubdate"/>
        <xsl:apply-templates select="title"/>
    </div>
</xsl:template>

<xsl:template match="volumenum">
    <p>
        <xsl:value-of select="."/>
    </p>
</xsl:template>

<xsl:template match="issuenum">
    <p>
        <xsl:value-of select="."/>
    </p>
</xsl:template>

<xsl:template match="publisher">
    <xsl:apply-templates select="publishername"/>
</xsl:template>

<xsl:template match="publishername">
    <p>
        <xsl:value-of select="."/>
    </p>
</xsl:template>

<xsl:template match="pubdate">
    <p>
        <xsl:value-of select="."/>
    </p>
</xsl:template>

<xsl:template match="title">
    <h1>
        <xsl:value-of select="."/>
    </h1>
</xsl:template>

<xsl:template match="sect1">
    <div>
        <xsl:apply-templates select="title"/>
        <xsl:apply-templates select="para"/>
        <xsl:apply-templates select="sect2"/>
    </div>
</xsl:template>

<xsl:template match="sect1/title">
    <h2>
        <xsl:value-of select="."/>
    </h2>
</xsl:template>

<xsl:template match="sect2">
    <div>
       <xsl:apply-templates select="title"/>
       <xsl:apply-templates select="para"/>
       <xsl:apply-templates select="itemizedlist"/>
    </div>
</xsl:template>

<xsl:template match="sect2/title">
    <h3>
        <xsl:value-of select="."/>
    </h3>
</xsl:template>

<xsl:template match="para">
    <p>
        <xsl:value-of select="."/>
    </p>
</xsl:template>

<xsl:template match="sect2/itemizedlist">
    <ul>
        <xsl:apply-templates select="listitem"/>
    </ul>
</xsl:template>

<xsl:template match="itemizedlist/listitem">
    <li>
        <xsl:apply-templates select="para"/>
    </li>
</xsl:template>

<xsl:template match="ulink">
    <a href="{@url}">
        <xsl:value-of select="."/>
    </a>
</xsl:template>

</xsl:stylesheet>
